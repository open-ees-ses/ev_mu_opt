# ev_mu_opt


The tool was created by Stefan Englberger at the Institute for Electrical Energy Storage Technology at the Technical University of Munich. For efficient optimization, the tool was designed in Mathworks Matlab and also supports the Gurobi solver.


### **How to cite:**

[Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.](https://doi.org/10.1016/j.apenergy.2021.117862)
