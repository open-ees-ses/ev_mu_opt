%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

%% Re-Calculate Reference Case Charging Power (Power for individual EV needed)
% Warning: This script is only viable for single entities and NMC cells as of now
refCaseChargingEnergy = zeros(length(Optimization.profit_sci), Input.entities, size(Input.power_drive,3));
refCaseChargingPower = zeros(length(Optimization.profit_sci), Input.entities, size(Input.power_drive,3));
refCaseRemainingEnergy = zeros(length(Optimization.profit_sci), Input.entities, size(Input.power_drive,3));
energyNominal = Input.EV.energy_nominal - Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min);
refCaseRemainingEnergy(1,:,:) = energyNominal .* Input.EV.state_of_charge_initial;

targetSOC = 1.00;

for iEntity = 1:Input.entities
    for iVehicle = 1:size(Input.power_drive,3)
        for iTimestep = 1:length(Optimization.profit_sci)-1
            if Input.ev_available(iTimestep, iEntity, iVehicle) && refCaseRemainingEnergy(iTimestep, iEntity, iVehicle) < energyNominal(iEntity, iVehicle) * targetSOC
                % Charge Vehicle
                energyDeficit = (energyNominal(iVehicle) * targetSOC) - refCaseRemainingEnergy(iTimestep, iEntity, iVehicle);
                refCaseChargingEnergy(iTimestep, iEntity, iVehicle)...
                    = min(Input.EV.power_charge(iVehicle) * Input.EV.efficiency_charge(iVehicle) * Input.sample_time, energyDeficit);
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle)...
                    + refCaseChargingEnergy(iTimestep, iEntity, iVehicle);
                % Calculate Power to charge vehicle
                refCaseChargingPower(iTimestep+1, iEntity, iVehicle) = min(Input.EV.power_charge(iVehicle),...
                    energyDeficit / (Input.sample_time * Input.EV.efficiency_charge(iVehicle)));
            elseif Input.power_drive(iTimestep, iEntity, iVehicle) > 0
                % Drive Vehicle
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle)...
                    - Input.power_drive(iTimestep, iEntity, iVehicle) * Input.sample_time;
            else
                % Vehicle on Standby
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle);
            end
        end
    end
end

%% Calculate total power profile per vehicle
drivingPower = squeeze(Input.power_drive);
refCaseChargingPower = squeeze(refCaseChargingPower);
refCaseRemainingEnergy = squeeze(refCaseRemainingEnergy);
refCaseTotalPower = refCaseChargingPower - drivingPower(1:length(refCaseChargingPower),:);

%% Calculate Aging
capLossCalendar = zeros(size(Optimization.capacity_loss_ev_calendar));
capLossCycle = zeros(size(Optimization.capacity_loss_ev_cycle));
for vehicle = 1:size(Input.power_drive,3)
    state_of_charge_ev = refCaseRemainingEnergy(:,vehicle) ./ Input.EV.energy_nominal(vehicle);
    state_of_charge_ev(isnan(state_of_charge_ev)) = 0;
    temperature_ev = Input.EV.temperature_cell;
    if strcmp(Input.EV.cell_chemistry(vehicle),'NMC')
        time = (0:1:(Input.simulation_time/Input.sample_time))' * Input.sample_time / 24;
        time_previous = 0;
        charge_throughput_previous = 0;
        [Q_loss_calendar,Q_loss_cycle] = script_22_soh_calculation_nmc(...
            state_of_charge_ev,...
            temperature_ev,...
            time,...
            time_previous,...
            charge_throughput_previous,...
            Input.EV.capacity_cell_nominal);
        capLossCalendar(:,vehicle) = Q_loss_calendar;
        capLossCycle(:,vehicle) = Q_loss_cycle;
        clear Q_loss*;
    end
end
capLossTotal = capLossCalendar + capLossCycle;
state_of_health_initial = ones(1, size(Input.power_drive,3));
socEv = squeeze(permute(repmat(Input.EV.state_of_health_initial,1,1,Input.simulation_time/Input.sample_time),[3,1,2])) - cumsum(capLossTotal,1);

clearvars -except Optimization Input socEv