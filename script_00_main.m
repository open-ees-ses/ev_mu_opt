function [Input, Optimization] = script_00_main(config, iConfig, simName)
%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862
%
% Input:    config          table with simulation parameters from .csv file (only for batch sim)
%           iConfig         Index of current run in batch simulation
%           simName         Name of current simulation run for results
%                           subfolder (only for batch sim)
% Output:   Input           Struct with Input Parameters for saving results
%           Optimization    Struct with Optimization results
%
% Input Parameters can be left blank for single simulations

    % Initalize config variables as empty when not performing batch
    % simulation
    if ~exist('config', 'var')
        clear; close all; clc; tic;
        config = [];
        iConfig = [];
        simName = [];
    else
        clearvars -except config iConfig simName
        close all; clc; tic; 
    end
    
    rng('default')

%% initialization
    Input = script_10_initialization(config, iConfig); % data initialization

    run('script_11_load_profiles.m'); % load mu_opt data
    
%% add the path of the gurobi solver, if available
    if Input.gurobi_solver
        gurobipath = fileparts(which('gurobi'));
        if ~isempty(gurobipath)
            addpath([gurobipath(1:end-6),'examples\matlab']);
        else            
            warning('The Gurobi solver was not found in the MATLAB environment.');
        end
    end
    clear gurobipath; % clear unused variables

    run('script_20_optimization.m'); % optimization

%% additional variables
    efc_ev = cumsum(abs(diff(Optimization.energy_ev_actual ./ Optimization.state_of_health_ev ./ (2 * Input.EV.energy_nominal))));
    efc_sess = cumsum(abs(diff(Optimization.energy_sess_actual ./ Optimization.state_of_health_sess ./ (2 * Input.SESS.energy_nominal'))));
    avg_time_per_opt_loop = toc/(Input.simulation_time/Input.rolling_horizon);

    disp(['Average time per optimization loop: ',num2str(round(avg_time_per_opt_loop,2)),' seconds.']);

%% plotting
    run('script_40_plotting.m');

%% clean-up
    clearvars -except Input Optimization efc_* avg_*;
    delete('*.asv');

%% sort and adjust structs
    Input.PS.objective_weight = 1;
     Input = orderfields(Input);
        Input = rmfield(Input',{'active_fcr_st_smt','big_M','ev_available_2','fcr_power_bounds','gurobi_solver','intermediate_data','max_optimization_time','max_relative_optimization_gap','optimization_full','path_profile','plot_pdf','plot_png','profile_length','rolling_horizon','soh_caluclation','system_time','vehicles','price_ablav_surcharge','price_base_distribution','price_charges_total','price_concession_fee','price_eeg_surcharge','price_electricity_tax','price_grid_charges','price_kwkg_surcharge','price_offsu_surcharge','price_power_charges','price_strom_nev_surcharge','price_value_added_tax'});

     Optimization = orderfields(Optimization);
        Optimization = rmfield(Optimization',{'active_fcr','active_smt_purchase','active_smt_sell','capacity_loss_sess_calendar','capacity_loss_sess_cycle','capacity_loss_sess_total','capacity_loss_ev_total','costs_cycle_ev','costs_cycle_sess','energy_sess_actual','energy_sess_actual_btm','energy_sess_actual_ftm','energy_sess_ftm2btm','energy_sess_usable','energy_sess_usable_btm','energy_sess_usable_ftm','power_sess_charge','power_sess_charge_btm','power_sess_charge_ftm','power_sess_discharge','power_sess_discharge_btm','power_sess_discharge_ftm','energy_ev_buffer','energy_ev_buffer_lb','energy_ev_buffer_ub','power_btm_curtailment','power_drive_btm','state_of_health_sess','state_of_charge_sess','power_smt_sell_total','power_smt_purchase_total','power_fcr_allocation_total'});
        
end