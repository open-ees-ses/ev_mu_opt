function Input = script_10_initialization(config, iConfig)
%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

    Input.system_time = datestr(now,'yyyymmddHHMMSS');
    
    rand_ev_profile = [57,33,108,178,195,109,86,26,104,25,80,53,124,46,61,175,99,199,55,162,94,170,82,30,100,70,2,74,69,3,49,35,39,110,152,67,120,83,158,118,150,11,145,169,151,153,47,148,188,105,183,81,84,97,8,12,6,4,163,63,200,166,176,41,68,20,116,171,146,130,155,37,172,34,91,10,139,77,15,106,193,121,93,136,23,96,192,62,79,98,164,85,173,157,65,154,128,161,54,129,45,114,184,32,147,14,127,181,189,90,89,174,131,132,78,187,38,27,138,51,186,115,1,18,73,142,24,31,103,182,72,17,144,165,40,167,13,76,143,190,122,135,113,111,42,191,119,43,168,180,112,117,44,22,21,58,126,28,123,95,36,134,198,64,177,137,7,59,133,107,50,16,5,149,75,60,71,141,196,92,87,159,66,197,48,19,156,179,9,125,29,140,101,88,102,56,185,160,194,52];
    rand_ev_distance = [12800,13500,14300,15200,16700,14600,16900,15700,16800,15200,14000,12800,15200,16200,14600,14200,14500,13400,14300,14700,15100,14900,15900,15200,17700,15500,17700,13900,15800,14600,15900,15900,11700,13000,12800,15600,17200,14500,16200,15800,13400,15600,13900,17300,15000,17500,14400,15900,14900,12000,13500,15900,14900,13300,14100,15400,13500,16500,14000,17700,13400,15300,12700,13900,14100,15600,16400,15500,14400,16200,16200,15200,15900,15600,13500,16100,14000,14100,15300,14500,14800,15900,16600,14700,15500,14600,15300,15700,14100,15400,15900,15100,17600,14100,13900,12400,16400,16300,14900,16300,15300,15400,15200,15700,15200,19200,13300,12200,13300,13400,14300,14700,14700,15800,15600,16100,17700,16800,13100,11500,16400,12200,15100,15100,18300,14900,14200,15400,15400,15100,14100,13200,15500,13000,13500,17000,14400,14800,16300,14500,16500,14500,16500,15900,14700,13700,13400,14600,14300,14400,16500,14600,16700,14200,16500,14200,15300,16500,14400,14300,18000,16400,14400,16000,14500,16100,17100,12600,16500,17200,15100,17600,15200,13100,11700,14500,16100,15500,15600,14100,15200,12500,13900,13800,15800,15000,13300,15000,14000,14000,16300,15200,15600,16300,15300,15800,16000,16800,15700,17100];
    
    % general parameters
    Input.sample_time = 15 / 60; % [h] length of sample time
    Input.optimization_time = 24; % [h] length of optimization period
    Input.rolling_horizon = 8; % [h] length of rolling horizon period
    Input.simulation_time = config.simulation_time(iConfig) * 24; % [h] length of simulation period
    Input.simulation_start = 0 * 24; % [h] start time of simulation (effects profiles)
    Input.entities = config.entities(iConfig); % [1] number of entities
    Input.vehicles = config.vehicles(iConfig); % [1] number of maximum vehicles per entity
    
    rand_ev_profile = reshape(rand_ev_profile,Input.entities,[]);
    rand_ev_profile(:,Input.vehicles+1:end) = [];
    rand_ev_distance = reshape(rand_ev_distance,Input.entities,[]);
    rand_ev_distance(:,Input.vehicles+1:end) = [];

    Input.profile_length = (Input.simulation_time + Input.optimization_time - Input.rolling_horizon) / Input.sample_time;

    % enable/disable behind-the-meter (btm) and front-the-meter (ftm) applications
    Input.active_fcr = config.fcr(iConfig); % conduct frequency containment reserve (fcr, ftm)
    Input.active_ps = config.ps(iConfig); % conduct peak shaving (ps, btm)
    Input.active_sci = config.sci(iConfig); % conduct self-consumption increase (sci, btm)
    Input.active_smt = config.smt(iConfig); % conduct spot market trading (smt, ftm)
    Input.active_fcr_st_smt = ~Input.active_smt && Input.active_fcr; % allow scheduled transactions at smt
    
    Input.ftm2btm_driving = config.ftm2btm(iConfig); % true: ftm2btm energy shift is allowed for driving purposes only / false: ftm2btm energy shift can be used for driving, trading, etc.
    
    Input.active_pv(:,1) = double(logical(round(1.0-.5+rand(Input.entities,1),0))); % [binary] defines, if entity has pv system
    Input.active_sess(:,1) = Input.active_pv.*double(logical(round(0.0-.5+rand(Input.entities,1),0))); % [binary] defines, if entity has stationary energy storage system
    Input.active_ev(:,:) = double(logical(round(1.0-.5+rand(Input.entities,Input.vehicles),0))); % [binary] defines, if entity has electric vehicles
    
    Input.soh_caluclation = false; % state of health calculation
    Input.plot_png = true; % true, if plots should be exported as png files
    Input.plot_pdf = false; % true, if plots should be exported as pdf files
    Input.gurobi_solver = true; % true, if available gurobi solver should be used
    Input.optimization_full = false; % save full optimization data
    Input.path_profile = 'profiles.mat'; % path of profiles
    Input.big_M = 1e4; % [s] large number M (big M method)
    Input.max_optimization_time = 30; % [s] maximum time per optimization
    Input.max_relative_optimization_gap = 5e-3; % [1] maximum relative gap per optimization
    Input.intermediate_data = 0; % save intermediate results all x optimizatinon loops
    
    %% define profiles
    Input.number_power_load = randi([2,2],Input.entities,1); % profile number for load
    Input.number_pv = randi([5,5],size(Input.active_pv)) .* Input.active_pv; % profile number for pv
%     Input.number_ev_profile = randi([1,200],size(Input.active_ev)) .* Input.active_ev; % profile number for fcr price
    Input.number_ev_profile = rand_ev_profile .* Input.active_ev; % profile number for fcr price
    Input.number_price_fcr = 14 * Input.active_fcr; % profile number for fcr price
    Input.type_price_smt = 'idc'; % type of spot market ('dam' or 'idc')
    if Input.active_smt
        Input.number_price_smt_low = 46; % profile number for spot market price (low price signal)
        Input.number_price_smt_high = 47; % profile number for spot market price (high price signal)
    else
        Input.number_price_smt_low = 45; % profile number for spot market price (weighted average price signal)
        Input.number_price_smt_high = 45; % profile number for spot market price (weighted average price signal)
    end
    Input.number_frequency = 8 * Input.active_fcr; % profile number for frequency
    
    %% entity electricity demand and supply
    Input.consumption_building = 500000 * ones(Input.entities,1); % [kWh] annual energy consumption of building
    Input.peak_generation = 120 * Input.active_pv; % [kW] peak power of generation profile
    
    %% electric vehicle energy storage system (ev)
    Input.EV.state_of_health_initial = 1.0 * Input.active_ev; % [1] initial state of health of storage system
    Input.EV.cell_chemistry = repmat({'NMC'},Input.entities,Input.vehicles); % [NMC/LFP] battery cell chemistry
    Input.EV.avg_consumption = 0.25 .* Input.active_ev; % [kWh/km] average energy consumption per km
%     Input.EV.avg_distance = round(normrnd(136,15,[Input.entities,Input.vehicles]))*100 .* Input.active_ev; % [km] annual driving distance
    Input.EV.avg_distance = rand_ev_distance .* Input.active_ev; % [km] annual driving distance
    Input.EV.energy_nominal = config.energy_nominal(iConfig) .* Input.active_ev; % [kWh] nominal energy content of storage system
    Input.EV.state_of_charge_min = 0.00 * Input.active_ev; % [1] minimum state of charge (lower bound)
    Input.EV.state_of_charge_max = 1.00 * Input.active_ev; % [1] maximum state of charge (upper bound)
    Input.EV.power = config.power(iConfig) .* Input.active_ev; % [kW] rated active power per inverter
    Input.EV.state_of_charge_initial = config.soc(iConfig)/100 * Input.active_ev; % [1] state of charge at the begin of simulation
    Input.EV.state_of_charge_end = Input.EV.state_of_charge_initial; % [1] state of charge at the end of simulation
    Input.EV.soc_preference = 0.20 * Input.active_ev; % [1] minimum soc threshold that should be allowed (max charge rate)
    Input.EV.efficiency_ev = 0.894 * Input.active_ev; % [1] efficiency of battery cells
    Input.EV.efficiency_charger = 1.00 * Input.active_ev; % [1] efficiency of power electronics
    Input.EV.efficiency_charge = Input.EV.efficiency_ev .* Input.EV.efficiency_charger; % [1] efficiency during charging
    Input.EV.efficiency_discharge = Input.EV.efficiency_ev .* Input.EV.efficiency_charger; % [1] efficiency during discharging
    Input.EV.power_charge = min(1.00*Input.EV.energy_nominal/1,Input.EV.power); % [kW] maximum active power during charging of ESS
    if config.bidirectional(iConfig)
        Input.EV.power_discharge = min(1.00*Input.EV.energy_nominal/1,Input.EV.power); % [kW] maximum active power during charging of ESS
    else
        Input.EV.power_discharge = min(0*Input.EV.energy_nominal/1,Input.EV.power); % [kW] maximum active power during charging of ESS
    end
    Input.EV.energy_self_discharge = 0 * Input.EV.energy_nominal; % self-discharge depending on nominal energy content
    Input.EV.energy_shift = config.energy_shift(iConfig) .* Input.EV.energy_nominal; % [kWh] maximum energy exchange between ftm and btm partition
    Input.EV.invest_battery = config.invest_battery(iConfig) * Input.EV.energy_nominal; % [EUR] invest costs for storage system
    Input.EV.efc_capability = 1000 * Input.active_ev; % [1] equivalent full cycles capability until system's end of life
    Input.EV.temperature_cell = 15 + 273.15; % [K] cell temperature
    Input.EV.capacity_cell_nominal = 2.05; % [Ah] nominal cell capacity
    Input.EV.ref_case_target_soc = 1.00; % [1] SOC to charge towards in reference profile for ps/sci profit
    
    %% stationary energy storage system (sess)
    Input.SESS.state_of_health_initial = 1.0 * Input.active_sess; % [1] initial state of health of storage system
    Input.SESS.cell_chemistry = repmat({'LFP'},Input.entities,1); % [NMC/LFP] battery cell chemistry
    Input.SESS.energy_nominal = 5*randi([1,3],size(Input.active_sess)) .* Input.active_sess; % [kWh] nominal energy content of storage system
    Input.SESS.state_of_charge_min = 0.05 * Input.active_sess; % [1] minimum state of charge (lower bound)
    Input.SESS.state_of_charge_max = 0.95 * Input.active_sess; % [1] maximum state of charge (upper bound)
    Input.SESS.power = 0.6 * Input.SESS.energy_nominal; % [kW] rated active power per inverter
    Input.SESS.state_of_charge_initial = 0.50 * Input.active_sess; % [1] state of charge at the begin of simulation
    Input.SESS.state_of_charge_end = 0.50 * Input.active_sess; % [1] state of charge at the end of simulation
    Input.SESS.efficiency_battery = 0.99 * Input.active_sess; % [1] efficiency of battery cells
    Input.SESS.efficiency_inverter = 0.96 * Input.active_sess; % [1] efficiency of power electronics
    Input.SESS.efficiency_charge = Input.SESS.efficiency_battery .* Input.SESS.efficiency_inverter; % [1] efficiency during charging
    Input.SESS.efficiency_discharge = Input.SESS.efficiency_battery .* Input.SESS.efficiency_inverter; % [1] efficiency during discharging
    Input.SESS.power_charge = min(1.00*Input.SESS.energy_nominal/1,Input.SESS.power); % [kW] maximum active power during charging of ESS
    Input.SESS.power_discharge = min(1.00*Input.SESS.energy_nominal/1,Input.SESS.power); % [kW] maximum active power during charging of ESS
    Input.SESS.energy_self_discharge = 0.015/(30.5*24/Input.sample_time) * Input.SESS.energy_nominal; % self-discharge depending on nominal energy content
    Input.SESS.energy_shift = 0 .* Input.SESS.energy_nominal; % [kWh] maximum energy exchange between ftm and btm partition
    Input.SESS.invest_battery = 700 * Input.SESS.energy_nominal; % [EUR] invest costs for storage system
    Input.SESS.efc_capability = 3000 * Input.active_sess; % [1] equivalent full cycles capability until system's end of life
    Input.SESS.temperature_cell = 20 + 273.15; % [K] cell temperature
    Input.SESS.capacity_cell_nominal = 2.05; % [Ah] nominal cell capacity
    
    %% price signals
    % https://www.bdew.de/media/documents/200730_BDEW-Strompreisanalyse_Juli_2020.pdf
    Input.price_base_distribution = 0.09170; % [EUR/kWh] base price for electricity distribution
    Input.price_grid_charges = 0.01750; % [EUR/kWh] energy related grid charges
    Input.price_value_added_tax = 0.00000; % [EUR/kWh] value added tax (https://www.bdew.de/media/documents/200730_BDEW-Strompreisanalyse_Juli_2020.pdf)
    Input.price_concession_fee = 0.00110; % [EUR/kWh] concession fee in Germany (http://www.gesetze-im-internet.de/kav/__2.html)
    Input.price_eeg_surcharge = 0.06500; % [EUR/kWh] eeg surcharge in Germany (https://www.bundesnetzagentur.de/DE/Sachgebiete/ElektrizitaetundGas/Verbraucher/PreiseRechnTarife/preiseundRechnungen-node.html)
    Input.price_kwkg_surcharge = 0.00254; % [EUR/kWh] kwkg surcharge in Germany (http://www.gesetze-im-internet.de/kwkg_2016/)
    Input.price_strom_nev_surcharge = 0.00270; % [EUR/kWh] strom nev surcharge (paragraph 19 Strom NEV) in Germany (https://www.gesetze-im-internet.de/stromnev/__19.html)
    Input.price_offsu_surcharge = 0.00395; % [EUR/kWh] offshore liability levy in Germany (https://www.gesetze-im-internet.de/enwg_2005/__17f.html)
    Input.price_ablav_surcharge = 0.00009; % [EUR/kWh] surcharge for interruptible loads in Germany (Abschaltverordnung AbLaV) (https://www.gesetze-im-internet.de/ablav_2016/__18.html)
    Input.price_electricity_tax = 0.01537; % [EUR/kWh] electricity tax in Germany
    Input.price_power_charges = 100.0; % [EUR/kW] power related grid charges
    Input.price_charges_total = Input.price_grid_charges ...
                                          + Input.price_value_added_tax ...
                                          + Input.price_concession_fee ...
                                          + Input.price_eeg_surcharge ...
                                          + Input.price_kwkg_surcharge ...
                                          + Input.price_strom_nev_surcharge ...
                                          + Input.price_offsu_surcharge ...
                                          + Input.price_ablav_surcharge ...
                                          + Input.price_electricity_tax; % [EUR/kWh]
    
    %% application specific settings
    % frequency containment reserve (fcr) settings
    Input.FCR.objective_weight = 1 * Input.active_fcr; % [1] relative weighting of the application in the objective function
    Input.FCR.period = 4; % [h] length of minimum period for fcr provision (must be multiple of sample time)
    Input.FCR.power_minimum = 0 * sum(Input.active_ev); % [kW] power increment of provided power
    Input.FCR.reserve_time = 15/60; % [h] reserved time for additional energy criteria
    Input.FCR.reserve_power = 0.25; % [1] reserved power for additional power criteria
    Input.FCR.frequency_nominal = 50.0; % [Hz] nominal frequency
    Input.FCR.frequency_deadband = 0.01; % [Hz] frequency dead band of fcr provision
    Input.FCR.frequency_droop = 0.4/100; % [1] frequency droop
    Input.FCR.overfulfillment = 1.2; % [1] maximum overfulfillment
    
    % peak shaving (ps) settings
    Input.PS.objective_weight = 1 * Input.active_ps; % [1] relative weighting of the application in the objective function
    Input.PS.price = Input.price_power_charges * ones(Input.entities,1); % [EUR/kW] electricity tariff for peak power
    Input.PS.billing_period = 365 * 24; % [h] billing period for peak power threshold
    Input.PS.power_threshold_initial = 0 * ones(Input.entities,1); % [kW] initial peak shave threshold (for each billing period)
    Input.PS.reset_threshold = true; % reset power threshold to initial value for each billing period
    if config.ps_power(iConfig) > 0
        Input.PS.objective_weight = 0 * Input.active_ps; % [1] relative weighting of the application in the objective function
        Input.PS.billing_period = 365 * 24; % [h] billing period for peak power threshold
        Input.PS.power_threshold_initial = config.ps_power(iConfig) * ones(Input.entities,1); % [kW] initial peak shave threshold (for each billing period)
    end
    
    % self-consumption increase (sci) settings
    Input.SCI.objective_weight = 1 * Input.active_sci; % [1] relative weighting of the application in the objective function
    Input.SCI.grid_tariff = (Input.price_base_distribution + Input.price_charges_total) * ones(Input.profile_length,1); % [EUR/kWh] electricity tariff for energy purchase
    Input.SCI.grid_remuneration = 0.03 * ones(Input.profile_length,1); % [EUR/kWh] remuneration price for energy sale
    Input.SCI.feedin_limit = 1.0 * Input.peak_generation; % [kW] feed-in limit power
    Input.SCI.price_selfconsumed = 0.0 * Input.price_eeg_surcharge;
    
    % spot market trading (smt) settings
    Input.SMT.objective_weight = 1 * Input.active_smt; % [1] relative weighting of the application in the objective function
    Input.SMT.period = 15/60; % [h] length of minimum period (must be multiple of sample time)
    Input.SMT.power_minimum = 0 * sum(Input.active_ev); % [kW] power increment of traded power
    Input.SMT.exclusive_trade = true; % true, if sell and purchase are exclusive
    Input.SMT.charges_purchase = 0; % [EUR/kWh] charges for purchased energy
    Input.SMT.charges_sell = 0; % [EUR/kWh] charges for sold energy

end