%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

tic
loopTime = zeros(Input.simulation_time/Input.rolling_horizon,1);
for optimization_loop = uint32(1:Input.simulation_time/Input.rolling_horizon)

    tStart = tic;
%% define optimization profiles and parameters
    power_pv = Input.power_pv( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    power_load = Input.power_load( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    power_drive = Input.power_drive( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:,:);
    ev_drive = power_drive; ev_drive(ev_drive>0) = 1;
    ev_available = Input.ev_available; ev_available(ev_available>1) = 1;
%     ev_available = Input.ev_available+Input.ev_available_2; ev_available(ev_available>1) = 1;
    ev_available = ev_available( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:,:);
    price_fcr = Input.price_fcr( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    price_smt = Input.price_smt( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    price_purchase_btm = Input.SCI.grid_tariff( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    price_sell_btm = Input.SCI.grid_remuneration( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    fcr_power_bounds = Input.fcr_power_bounds( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);    
    % Adjust temperatures when using temperature profiles instead of scalar
    % values
    if length(Input.EV.temperature_cell) == 1
        temperature_cell_sess = repmat(Input.SESS.temperature_cell,1,length(fcr_power_bounds));
    else
        temperature_cell_sess = Input.SESS.temperature_cell( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    end
    if length(Input.EV.temperature_cell) == 1
        temperature_cell_ev = repmat(Input.EV.temperature_cell,1,length(fcr_power_bounds));
    else
        temperature_cell_ev = Input.EV.temperature_cell( (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+1 : (optimization_loop-1)*Input.rolling_horizon/Input.sample_time+Input.optimization_time/Input.sample_time ,:);
    end

    if optimization_loop == 1
        state_of_health_ev = Input.EV.state_of_health_initial;
        state_of_health_sess = Input.SESS.state_of_health_initial;
        energy_ev_usable = Input.EV.energy_nominal .* state_of_health_ev - Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min);
        energy_sess_usable = Input.SESS.energy_nominal .* state_of_health_sess - Input.SESS.energy_nominal .* (1 - Input.SESS.state_of_charge_max + Input.SESS.state_of_charge_min);
        energy_ev_actual_initial = energy_ev_usable .* Input.EV.state_of_charge_initial;
        energy_sess_actual_initial = energy_sess_usable .* Input.SESS.state_of_charge_initial;
        power_btm_purchase_peak_max = Input.PS.power_threshold_initial;
    elseif Input.PS.reset_threshold && mod(double(optimization_loop-1)*Input.rolling_horizon,Input.PS.billing_period) == 0
        state_of_health_ev = reshape(Opt.state_of_health_ev(end,:,:),Input.entities,Input.vehicles);
        state_of_health_sess = Opt.state_of_health_sess(end,:)';
        energy_ev_usable = Input.EV.energy_nominal .* state_of_health_ev - Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min);
        energy_sess_usable = Input.SESS.energy_nominal .* state_of_health_sess - Input.SESS.energy_nominal .* (1 - Input.SESS.state_of_charge_max + Input.SESS.state_of_charge_min);
        power_btm_purchase_peak_max = Input.PS.power_threshold_initial;
    else
        state_of_health_ev = reshape(Opt.state_of_health_ev(end,:,:),Input.entities,Input.vehicles);
        state_of_health_sess = Opt.state_of_health_sess(end,:)';
        energy_ev_usable = Input.EV.energy_nominal .* state_of_health_ev - Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min);
        energy_sess_usable = Input.SESS.energy_nominal .* state_of_health_sess - Input.SESS.energy_nominal .* (1 - Input.SESS.state_of_charge_max + Input.SESS.state_of_charge_min);
        power_btm_purchase_peak_max = max(Input.PS.power_threshold_initial,Opt.power_btm_purchase_peak(end,:)');
    end

%% define decision variables
        energy_ev_ftm2btm = optimvar('energy_ev_ftm2btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(Input.EV.energy_shift,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_ev_actual_btm = optimvar('energy_ev_actual_btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_ev_actual_ftm = optimvar('energy_ev_actual_ftm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_ev_usable_btm = optimvar('energy_ev_usable_btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_ev_usable_ftm = optimvar('energy_ev_usable_ftm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_charge_btm = optimvar('power_ev_charge_btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_charge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_charge_ftm = optimvar('power_ev_charge_ftm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_charge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_discharge_btm = optimvar('power_ev_discharge_btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_discharge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_discharge_ftm = optimvar('power_ev_discharge_ftm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_discharge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_ev_buffer = optimvar('energy_ev_buffer', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0); % [kWh] buffer energy of electric vehicle for soc preference
        energy_ev_buffer_end = optimvar('energy_ev_buffer_end', 1,Input.entities,Input.vehicles,'LowerBound',0); % [kWh] buffer energy of electric vehicle for soc end
        power_drive_btm = optimvar('power_drive_btm', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0);
        energy_ev_buffer_lb = optimvar('energy_ev_buffer_lb', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0); % [kWh] buffer energy lower bound
        energy_ev_buffer_ub = optimvar('energy_ev_buffer_ub', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0); % [kWh] buffer energy upper bound

        energy_sess_ftm2btm = optimvar('energy_sess_ftm2btm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.energy_shift',Input.optimization_time/Input.sample_time,1));
        energy_sess_actual_btm = optimvar('energy_sess_actual_btm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1));
        energy_sess_actual_ftm = optimvar('energy_sess_actual_ftm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1));
        energy_sess_usable_btm = optimvar('energy_sess_usable_btm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1));
        energy_sess_usable_ftm = optimvar('energy_sess_usable_ftm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1));
        power_sess_charge_btm = optimvar('power_sess_charge_btm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_charge',Input.optimization_time/Input.sample_time,1));
        power_sess_charge_ftm = optimvar('power_sess_charge_ftm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_charge',Input.optimization_time/Input.sample_time,1));
        power_sess_discharge_btm = optimvar('power_sess_discharge_btm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_discharge',Input.optimization_time/Input.sample_time,1));
        power_sess_discharge_ftm = optimvar('power_sess_discharge_ftm', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_discharge',Input.optimization_time/Input.sample_time,1));

        energy_ev_actual = optimvar('energy_ev_actual', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_charge = optimvar('power_ev_charge', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_charge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        power_ev_charge_extern = optimvar('power_ev_charge_extern', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',(1-ev_available) .* (1-ev_drive) .* 11);
        power_ev_charge_extern_drive = optimvar('power_ev_charge_extern_drive', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_drive .* 150);
        power_ev_discharge = optimvar('power_ev_discharge', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0,'UpperBound',ev_available .* repmat(reshape(Input.EV.power_discharge,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1));
        energy_sess_actual = optimvar('energy_sess_actual', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1));
        power_sess_charge = optimvar('power_sess_charge', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_charge',Input.optimization_time/Input.sample_time,1));
        power_sess_discharge = optimvar('power_sess_discharge', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',repmat(Input.SESS.power_discharge',Input.optimization_time/Input.sample_time,1));

        power_btm_sell = optimvar('power_btm_sell', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);
        power_btm_purchase = optimvar('power_btm_purchase', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);
        power_btm_purchase_peak = optimvar('power_btm_purchase_peak', 1,Input.entities,'LowerBound',power_btm_purchase_peak_max);
        power_btm_curtailment = optimvar('power_btm_curtailment', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0);
        power_ftm_curtailment = optimvar('power_ftm_curtailment', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0,'UpperBound',0);
        power_fcr_actual = optimvar('power_fcr_actual', Input.optimization_time/Input.sample_time,Input.entities);
        power_fcr_allocation = optimvar('power_fcr_allocation', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);
        power_fcr_allocation_total = optimvar('power_fcr_allocation_total', Input.optimization_time/Input.sample_time,1,'LowerBound',0);
        power_smt_sell = optimvar('power_smt_sell', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);
        power_smt_sell_total = optimvar('power_smt_sell_total', Input.optimization_time/Input.sample_time,1,'LowerBound',0);
        power_smt_purchase = optimvar('power_smt_purchase', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);
        power_smt_purchase_total = optimvar('power_smt_purchase_total', Input.optimization_time/Input.sample_time,1,'LowerBound',0);

        active_fcr = optimvar('active_fcr', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1);
        increment_fcr = optimvar('increment_fcr', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0);

        active_smt_sell = optimvar('active_smt_sell', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1);
        active_smt_purchase = optimvar('active_smt_purchase', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0,'UpperBound',1);
        increment_smt_sell = optimvar('increment_smt_sell', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0);
        increment_smt_purchase = optimvar('increment_smt_purchase', Input.optimization_time/Input.sample_time,1,'Type','integer','LowerBound',0);

        profit_fcr = optimvar('profit_fcr', Input.optimization_time/Input.sample_time,Input.entities);
        profit_smt = optimvar('profit_smt', Input.optimization_time/Input.sample_time,Input.entities);
        profit_sci = optimvar('profit_sci', Input.optimization_time/Input.sample_time,Input.entities);
        profit_ps = optimvar('profit_ps', 1,Input.entities);
        costs_cycle_ev = optimvar('costs_cycle_ev', Input.optimization_time/Input.sample_time,Input.entities,Input.vehicles,'LowerBound',0);
        costs_cycle_sess = optimvar('costs_cycle_sess', Input.optimization_time/Input.sample_time,Input.entities,'LowerBound',0);

%% objective function with underlying profit functions
    dummy_sess_eff_ch = 1./Input.SESS.efficiency_charge; dummy_sess_eff_ch(dummy_sess_eff_ch==Inf) = 0;
    dummy_ev_eff_ch = 1./Input.EV.efficiency_charge; dummy_ev_eff_ch(dummy_ev_eff_ch==Inf) = 0;
    dummy_sess_eff_dch = 1./Input.SESS.efficiency_discharge; dummy_sess_eff_dch(dummy_sess_eff_dch==Inf) = 0;
    dummy_ev_eff_dch = 1./Input.EV.efficiency_discharge; dummy_ev_eff_dch(dummy_ev_eff_dch==Inf) = 0;
    
    opt_problem = optimproblem('ObjectiveSense','maximize','Objective',...
        Input.FCR.objective_weight * sum(sum(profit_fcr)) ...
        + Input.PS.objective_weight * sum(profit_ps) ...
        + Input.SCI.objective_weight * sum(sum(profit_sci)) ...
        + Input.SMT.objective_weight * sum(sum(profit_smt)) ...
        - sum(sum(energy_sess_ftm2btm,1) .* dummy_sess_eff_ch' ) * (Input.price_charges_total) ...
        - sum(sum(squeeze(sum(energy_ev_ftm2btm,1)) .* dummy_ev_eff_ch' )) * (Input.price_charges_total) ...
        - 1e0 * sum(sum(sum(costs_cycle_ev))) ...
        - 1e0 * sum(sum(costs_cycle_sess)) ...
        - 1e2 * sum(sum(sum(power_ev_charge_extern*Input.sample_time))) ...
        - 1e3 * sum(sum(sum(power_ev_charge_extern_drive*Input.sample_time))) ...
        - 1e0 * sum(sum(sum(energy_ev_buffer))) ...
        - 1e0 * sum(sum(energy_ev_buffer_end)) * Input.optimization_time/Input.sample_time ...
        - 0e1 * sum(sum(sum(energy_ev_buffer_lb))) ...
        - 0e1 * sum(sum(sum(energy_ev_buffer_ub))) );

% call constraints
    run('script_21_constraints.m');

%% solve optimization
% define solver options
    opt_options = optimoptions('intlinprog','Display','none','MaxTime',Input.max_optimization_time,'RelativeGapTolerance',Input.max_relative_optimization_gap);

% call solver
    [opt_variables,opt_objective,opt_flag,opt_settings] = solve(opt_problem,'Options',opt_options);

% check optimization success
    if opt_flag < 1; error('Optimization was not successful'); end

% create optimization container
    Optimization = opt_variables;
    
    Optimization.energy_ev_usable = reshape(energy_ev_usable,1,Input.entities,Input.vehicles);
    Optimization.energy_sess_usable = energy_sess_usable';

% correction of peak shaving threshold for relevant time period
    Optimization.power_btm_purchase_peak = max(power_btm_purchase_peak_max,...
        max(Optimization.power_btm_purchase(1:Input.rolling_horizon/Input.sample_time)-Optimization.power_btm_sell(1:Input.rolling_horizon/Input.sample_time)))';

% save all optimization results
    if Input.optimization_full && Input.simulation_time/Input.rolling_horizon > 1
        Optimization_full(optimization_loop) = Optimization;
    end

%% clear unused variables
    clear active* cost* energy* ev* fcr* opt_* power* price* profit*;    

%% correction of decision variables (vectors with uniform dimensions)
    fields = fieldnames(Optimization);
    for i1 = uint32(1:length(fields))
        field = ['Optimization.',fields{i1}];
        field_dummy = eval(field);
        if size(field_dummy,1) ~= Input.optimization_time/Input.sample_time
            eval([field,' = repelem(',field,',(Input.optimization_time/Input.sample_time)/size(field_dummy,1),1);']);
        end
    end
    clear field* i1;

%% save only data during the rolling horizon
    fields = fieldnames(Optimization);
    for i1 = uint32(1:length(fields))
        Optimization.(fields{i1})(Input.rolling_horizon/Input.sample_time+1:end,:,:) = [];
    end
    clear field* i1;
    
%% calculate actual energy content and state of charge on system level
    Optimization.energy_ev_actual = Optimization.energy_ev_actual + repmat(reshape(Input.EV.energy_nominal .* Input.EV.state_of_charge_min,1,Input.entities,Input.vehicles),Input.rolling_horizon/Input.sample_time,1,1);
    Optimization.state_of_charge_ev = Optimization.energy_ev_actual ...
        ./ ( Optimization.energy_ev_usable + repmat(reshape(Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min),1,Input.entities,Input.vehicles),Input.rolling_horizon/Input.sample_time,1,1) );
    Optimization.state_of_charge_ev(isnan(Optimization.state_of_charge_ev)) = 0;

    Optimization.energy_sess_actual = Optimization.energy_sess_actual + repmat(Input.SESS.energy_nominal .* Input.SESS.state_of_charge_min,1,Input.rolling_horizon/Input.sample_time)';
    Optimization.state_of_charge_sess = Optimization.energy_sess_actual ...
        ./ ( Optimization.energy_sess_usable + repmat(Input.SESS.energy_nominal .* (1 - Input.SESS.state_of_charge_max + Input.SESS.state_of_charge_min),1,Input.rolling_horizon/Input.sample_time)' );
    Optimization.state_of_charge_sess(isnan(Optimization.state_of_charge_sess)) = 0;

%% call script for the state of health calculation
    run('script_22_soh_calculation.m');

%% concatenate results from optimizations
    fields = fieldnames(Optimization);
    for i1 = uint32(1:length(fields))
        if optimization_loop == 1
            Opt.(fields{i1}) = Optimization.(fields{i1});
        else
            Opt.(fields{i1}) = [Opt.(fields{i1});Optimization.(fields{i1})];
        end
    end
    clear Optimization field* i1;
    
% show optimization progress    
    loopTime(optimization_loop) = toc(tStart);
    remainingTimeEstimate = (round(Input.simulation_time/Input.rolling_horizon,0) - optimization_loop) * mean(loopTime(loopTime>0));
    remainingTimeEstimate = remainingTimeEstimate / 60;
    disp(['[ ',num2str(optimization_loop),' / ',num2str(round(Input.simulation_time/Input.rolling_horizon,0)),...
        ' ] optimizations completed - Remaining Time: ', num2str(remainingTimeEstimate), 'min', newline]);
end

% clear unused variables
    clear charge* time* state*;
    
%% correction of decision variables (squeeze dimensions)
    fields = fieldnames(Opt);
    for i1 = uint32(1:length(fields))
        field = ['Opt.',fields{i1}];
        field_dummy = eval(field);
        eval([field,' = squeeze(',field,');']);
    end
    clear field* i1;

%% correct profit from frequency containment reserve
    Opt.profit_fcr = cumsum(Opt.profit_fcr);

%% correct profit from spot market trading
    Opt.profit_smt = cumsum(Opt.profit_smt);
    
%% Calculate Reference Scenario for SCI (Always charge to 100% SOC)
refCaseChargingEnergy = zeros(length(Opt.profit_sci), Input.entities, Input.vehicles);
refCaseChargingPower = zeros(length(Opt.profit_sci), Input.entities, Input.vehicles);
refCaseRemainingEnergy = zeros(length(Opt.profit_sci), Input.entities, Input.vehicles);
energyNominal = Input.EV.energy_nominal - Input.EV.energy_nominal .* (1 - Input.EV.state_of_charge_max + Input.EV.state_of_charge_min);
refCaseRemainingEnergy(1,:,:) = energyNominal .* Input.EV.state_of_charge_initial;
refCaseChargingCost = zeros(length(Opt.profit_sci), Input.entities, Input.vehicles);

for iEntity = 1:Input.entities
    for iVehicle = 1:Input.vehicles 
        for iTimestep = 1:length(Opt.profit_sci)-1
            if Input.ev_available(iTimestep, iEntity, iVehicle) && refCaseRemainingEnergy(iTimestep, iEntity, iVehicle) < (energyNominal(iEntity, iVehicle) * Input.EV.ref_case_target_soc)
                % Charge Vehicle
                energyDeficit = (energyNominal(iVehicle) * Input.EV.ref_case_target_soc) - refCaseRemainingEnergy(iTimestep, iEntity, iVehicle);
                refCaseChargingEnergy(iTimestep, iEntity, iVehicle)...
 = min(Input.EV.power_charge(iVehicle) * Input.EV.efficiency_charge(iVehicle) * Input.sample_time, energyDeficit);
                refCaseChargingCost(iTimestep, iEntity, iVehicle) = refCaseChargingEnergy(iTimestep, iEntity, iVehicle) * Input.SCI.grid_tariff(iTimestep);
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle)...
                    + refCaseChargingEnergy(iTimestep, iEntity, iVehicle);
                % Calculate Power to charge vehicle
                refCaseChargingPower(iTimestep+1, iEntity, iVehicle) = min(Input.EV.power_charge(iVehicle),...
                    energyDeficit / (Input.sample_time * Input.EV.efficiency_charge(iVehicle)));
            elseif Input.power_drive(iTimestep, iEntity, iVehicle) > 0
                % Drive Vehicle
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle)...
                    - Input.power_drive(iTimestep, iEntity, iVehicle) * Input.sample_time;
            else
                % Vehicle on Standby
                refCaseRemainingEnergy(iTimestep+1, iEntity, iVehicle) = refCaseRemainingEnergy(iTimestep, iEntity, iVehicle);
            end
        end
    end
end
refCaseChargingCost = squeeze(refCaseChargingCost);
refCaseChargingCost = -cumsum(refCaseChargingCost, 1);

refCaseChargingPower = sum(refCaseChargingPower, 3); % Power Sum for Entity/Aggregator
refCaseChargingPower = squeeze(refCaseChargingPower);
Opt.power_ev_charge_btm_ref = refCaseChargingPower;

refCaseRemainingEnergy = squeeze(refCaseRemainingEnergy);
refCaseRemainingEnergy(refCaseRemainingEnergy < 0) = 0;
refCaseRemainingSOC = refCaseRemainingEnergy ./ Input.EV.energy_nominal;
Opt.state_of_charge_ev_ref = refCaseRemainingSOC;
Opt.energy_ev_actual_ref = refCaseRemainingEnergy;

%% correct profit from self-consumption increase
    power_residual = Input.power_load(1:Input.simulation_time/Input.sample_time,:)...
        + refCaseChargingPower(1:Input.simulation_time/Input.sample_time,:)...
        - Input.power_pv(1:Input.simulation_time/Input.sample_time,:);
    power_residual_pos = power_residual; power_residual_pos(power_residual_pos < 0) = 0; % Additional Power needed
    power_residual_neg = power_residual; power_residual_neg(power_residual_neg > 0) = 0; % Excess PV
    power_residual_neg = -power_residual_neg; % power_residual_neg(power_residual_neg > repmat(Input.SCI.feedin_limit',size(power_residual_neg,1),1)) = repmat(Input.SCI.feedin_limit',size(power_residual_neg,1),1);
    profit_no_ess_sci = (Input.SCI.grid_remuneration(1:Input.simulation_time/Input.sample_time,:) * Input.sample_time...
        .* power_residual_neg - Input.SCI.grid_tariff(1:Input.simulation_time/Input.sample_time,:) * Input.sample_time...
        .* power_residual_pos) - Input.SCI.price_selfconsumed * Input.sample_time...
        .* (Input.power_load(1:Input.simulation_time/Input.sample_time,:) - power_residual_pos);
    Opt.profit_sci = Input.SCI.grid_remuneration(1:Input.simulation_time/Input.sample_time,:) * Input.sample_time .* Opt.power_btm_sell - Input.SCI.grid_tariff(1:Input.simulation_time/Input.sample_time,:) * Input.sample_time .* (sum(Opt.power_btm_purchase,2) + sum(Opt.power_ev_charge_extern,2) + sum(Opt.power_ev_charge_extern_drive,2));
    Opt.profit_sci = cumsum(Opt.profit_sci - profit_no_ess_sci - Opt.energy_ev_ftm2btm / Input.EV.efficiency_charge * Input.price_charges_total);
    clear i1 profit_no_ess*;

%% correct profit from peak shaving
    % Calculate cost reduction over each billing period
    ps_periods = ceil(Input.simulation_time/Input.PS.billing_period);
    if ps_periods > 1
        power_residual_pos = [power_residual_pos',zeros(1,ps_periods*Input.PS.billing_period-length(power_residual_pos))];
        power_residual_pos = reshape(power_residual_pos,ps_periods,numel(power_residual_pos)/ps_periods);
        power_residual_pos = cummax(power_residual_pos,1);
        power_residual_pos = reshape(power_residual_pos,numel(power_residual_pos),1);
        power_residual_pos(length(power_residual)+1:end) = [];
    end

    % Remove Initial Peak 
    if length(power_residual_pos) > 50
        power_residual_pos(1:50) = 0;
    end
        
	Opt.profit_ps = (cummax(power_residual_pos) - Opt.power_btm_purchase_peak) .* Input.PS.price';
    
    % Apply final profit per billing period to entire respective period
    profit_billing_period = zeros(ps_periods, 1);
    for iPeriod = 1:ps_periods
        profit_billing_period(iPeriod) = Opt.profit_ps(min(iPeriod * Input.PS.billing_period, Input.simulation_time)...
            / Input.sample_time);
        % Generate final corrected PS profit profile
        if iPeriod == 1
            Opt.profit_ps((iPeriod-1)*Input.PS.billing_period/Input.sample_time+1 : min(iPeriod*Input.PS.billing_period, Input.simulation_time)/Input.sample_time)...
                = profit_billing_period(iPeriod);
        else
            Opt.profit_ps((iPeriod-1)*Input.PS.billing_period/Input.sample_time+1 : min(iPeriod*Input.PS.billing_period, Input.simulation_time)/Input.sample_time)...
            = profit_billing_period(iPeriod) + sum(profit_billing_period(1:iPeriod-1));
        end
    end
    
	clear power_residual* ps_periods*;
  
%% Calculate Total Profit
Opt.profit_total = Opt.profit_sci + Opt.profit_ps + Opt.profit_smt + Opt.profit_fcr;
    
%% rename concatenated struct
    Optimization = Opt;
    clear Opt;