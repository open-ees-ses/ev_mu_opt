%% Script Description
%  Coded and provided by:
%  Stefan Englberger (stefan.englberger@tum.de)
%  Institute for Electrical Energy Storage Technology
%  Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

    close all;

    mymap=[0.7796,0.6021,0;0.8396,0.3018,0;0.0048,0.4473,0.7503;0,0.7282,0.7897];
    fig_group_name = 'fs';

%% plot data
    lw = 1.3;
    set(0, 'DefaultLineLineWidth', lw);
    set(0, 'DefaultFigureRenderer', 'painters');
% define font
    font_name = 'Arial';
    font_size = 8;
    token_size = [15 10];
    leg_color = uint8(255*[1;1;1;.7]);
    subplot_size = [0.403, 0.285];
    subplot_x_coordinate = linspace(0.06,1-0.01-subplot_size(1),2);
    subplot_y_coordinate = linspace(0.08,1-0.005-subplot_size(2),3);

% create sampling vector for the x-axis (time in days)
    plot_sampling = Input.simulation_start/(24*1)+Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
        Input.simulation_time/(24*1)/(Input.simulation_time/Input.sample_time): ...
        Input.simulation_start/(24*1)+Input.simulation_time/(24*1);
    xlimtime = [Input.simulation_start/(24*1) (Input.simulation_start+Input.simulation_time)/(24*1)];
%   xlimtime = [0 2];

% initialize figure
    fig1 = figure('Name','Plots', 'Units','centimeters', 'Position',[5 5 17.2 9]);

% plot active power at ess over time
    ax1 = subplot(3,2,1); hold on; box on; grid on;
        p(1) = bar(plot_sampling,Input.power_load(1:Input.simulation_time/Input.sample_time)-Input.power_pv(1:Input.simulation_time/Input.sample_time),1, 'FaceColor','k', 'FaceAlpha',0.5, 'EdgeColor','none', 'DisplayName','P^{BTM,w/o ESS}');
        p(2) = bar(plot_sampling,Optimization.power_btm_purchase - Optimization.power_btm_sell,1, 'EdgeColor','none', 'Facecolor',mymap(2,:), 'Facealpha',0.7, 'DisplayName','P^{BTM, w/ ESS}');
        p(3) = stairs(plot_sampling,Optimization.power_btm_purchase_peak, 'Color',mymap(2,:), 'LineWidth',lw, 'DisplayName','P^{PS}');
        xlim(xlimtime); xticklabels([]);
        for idx = 1:length(p); if idx ==1; ymax = max(p(idx).YData); ymin = min(p(idx).YData); else; if ymax < max(p(idx).YData); ymax = max(p(idx).YData); end; if ymin > min(p(idx).YData); ymin = min(p(idx).YData); end;    end; end; if ymax < 0; ymax = ymax / 1.1; else; ymax = ymax * 1.1; end; if ymin > 0; ymin = ymin / 1.1; else; ymin = ymin * 1.1; end; clear p idx; ylim([ymin ymax]);
        leg = legend('Location','South', 'Orientation','horizontal'); ylabel('Power (kW)');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax1.Layer = 'top';

% plot cumulative profit over time
    ax2 = subplot(3,2,3); hold on; box on; grid on;
        yyaxis left; ylabel('Frequency (Hz)'); ylim([49.85 50.15]); plot(plot_sampling,Input.frequency(1:length(plot_sampling)),'Color','k', 'DisplayName','Frequency');
        yyaxis right; ylabel('Power (kW)'); p = plot(plot_sampling,Optimization.power_fcr_actual,'Color',mymap(3,:), 'DisplayName','FCR Power');
        for idx = 1:length(p); if idx ==1; ymax = max(p(idx).YData); ymin = min(p(idx).YData); else; if ymax < max(p(idx).YData); ymax = max(p(idx).YData); end; if ymin > min(p(idx).YData); ymin = min(p(idx).YData); end;    end; end; if ymax < 0; ymax = ymax / 1.1; else; ymax = ymax * 1.1; end; if ymin > 0; ymin = ymin / 1.1; else; ymin = ymin * 1.1; end; clear p idx; ylim([-max([abs(ymin),abs(ymax)])-0.01,max([abs(ymin),abs(ymax)])+0.01]);
        xlim(xlimtime); xticklabels([]);
        ax = gca;
            ax.YAxis(1).Color = 'k';
            ax.YAxis(2).Color = mymap(3,:);
        leg = legend('Location','South', 'Orientation','horizontal');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax2.Layer = 'top';

% plot state of charge and equivalent full cycles over time
    ax3 = subplot(3,2,5); hold on; box on; grid on;
        x = plot_sampling;
        y1 = transpose(Input.price_smt(1:length(plot_sampling),1)*1000);
        y2 = transpose(Input.price_smt(1:length(plot_sampling),2)*1000);
        yyaxis left; ylabel('Price (EUR/MWh)'); ylim([-120,120]); %stairs(plot_sampling,Input.price_smt_low(1:length(plot_sampling))*1000,'-', 'LineWidth',lw, 'DisplayName','Price low');
                                                                     %stairs(plot_sampling,Input.price_smt_high(1:length(plot_sampling))*1000,'-', 'LineWidth',lw, 'DisplayName','Price high');
                                                                     p = patch([x fliplr(x)], [y1 fliplr(y2)], 'g');
                                                                     p.FaceColor = 'k'; p.FaceAlpha = 0.3;
                                                                     p.EdgeColor = 'k'; p.EdgeAlpha = 0.4;
                                                                     p.LineWidth = lw;
        yyaxis right; ylabel('Power (kW)'); p = stairs(plot_sampling,Optimization.power_smt_sell-Optimization.power_smt_purchase,'-', 'Color',mymap(4,:), 'LineWidth',lw, 'DisplayName','SMT Power');
        for idx = 1:length(p); if idx ==1; ymax = max(p(idx).YData); ymin = min(p(idx).YData); else; if ymax < max(p(idx).YData); ymax = max(p(idx).YData); end; if ymin > min(p(idx).YData); ymin = min(p(idx).YData); end;    end; end; if ymax < 0; ymax = ymax / 1.1; else; ymax = ymax * 1.1; end; if ymin > 0; ymin = ymin / 1.1; else; ymin = ymin * 1.1; end; clear p idx; ylim([-max([abs(ymin),abs(ymax)])-0.01,max([abs(ymin),abs(ymax)])+0.01]);
        xlim(xlimtime);
        ax = gca;
            ax.YAxis(1).Color = 'k';
            ax.YAxis(2).Color = mymap(4,:);
        leg = legend({'SMT Price','SMT Power'}, 'Location','South', 'Orientation','horizontal');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax3.Layer = 'top';
        xlabel('Time (d)');
        
% plot input power profiles over time
    ax4 = subplot(3,2,2); hold on; box on; grid on;
        bar(plot_sampling,sum(-Optimization.power_ev_discharge_btm-Optimization.power_ev_discharge_ftm+Optimization.power_ev_charge_btm+Optimization.power_ev_charge_ftm,2),1, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
        bar(plot_sampling,sum(-Optimization.power_ev_discharge_btm+Optimization.power_ev_charge_btm,2),1, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
        xlim(xlimtime);  xticklabels([]); ylim([-sum(Input.EV.power_discharge)*1.1,sum(Input.EV.power_charge)*1.1]);
        leg = legend('Location','North', 'Orientation','horizontal'); ylabel('Charging Power / kW');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax4.Layer = 'top';    

% plot power allocation over time
    ax5 = subplot(3,2,4); hold on; box on; grid on;
        p(1) = area(plot_sampling,sum(Optimization.energy_ev_usable_btm+Optimization.energy_ev_usable_ftm,2)./sum(Optimization.energy_ev_usable_btm+Optimization.energy_ev_usable_ftm,2)*100, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
        p(2) = area(plot_sampling,sum(Optimization.energy_ev_usable_btm,2)./sum(Optimization.energy_ev_usable_btm+Optimization.energy_ev_usable_ftm,2)*100, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
        xlim(xlimtime); xticklabels([]); ylim([0 100]);
        leg = legend(p, 'Location','North', 'Orientation','horizontal'); ylabel('Reserved Energy (%)');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax5.Layer = 'top';
%       set(ax5, 'Units', 'normalized'); set(ax5, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(2), subplot_size]);
%       axis(ax5, 'off');

% plot energy allocation and state of health over time
    soh = mean(Optimization.state_of_health_ev,2)*100;
    [ d, idx ] = min( abs( soh-80 ) );
    ax6 = subplot(3,2,6); hold on; box on; grid on;
        ylabel('State of Charge (%)'); ylim([0 100]);
        area(plot_sampling,(sum(Optimization.energy_ev_actual_ftm,2)./sum(Input.EV.energy_nominal,2)+sum(Optimization.energy_ev_actual_btm,2)./sum(Input.EV.energy_nominal,2)+Input.EV.state_of_charge_min(1,1))*100, 'FaceColor',mean(mymap(3:4,:)), 'EdgeColor','none', 'DisplayName','FTM');
        area(plot_sampling,(sum(Optimization.energy_ev_actual_btm,2)./sum(Input.EV.energy_nominal,2)+Input.EV.state_of_charge_min(1,1))*100, 'FaceColor',mean(mymap(1:2,:)), 'EdgeColor','none', 'DisplayName','BTM');
        plot(plot_sampling,soh, 'Color','k', 'DisplayName','SOH');
        xline(idx/24*Input.sample_time, 'Color','k');
        xlim(xlimtime);
        leg = legend('Location','North', 'Orientation','horizontal'); xlabel('Time (d)');
        leg.ItemTokenSize = token_size; set(leg.BoxFace, 'ColorType','truecoloralpha', 'ColorData',leg_color);
        ax6.Layer = 'top';

        
        set(ax1, 'Units', 'normalized'); set(ax1, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(3), subplot_size]);
        set(ax2, 'Units', 'normalized'); set(ax2, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(2), subplot_size]);
        set(ax3, 'Units', 'normalized'); set(ax3, 'Position', [subplot_x_coordinate(1), subplot_y_coordinate(1), subplot_size]);
        set(ax4, 'Units', 'normalized'); set(ax4, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(3), subplot_size]);
        set(ax5, 'Units', 'normalized'); set(ax5, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(2), subplot_size]);
        set(ax6, 'Units', 'normalized'); set(ax6, 'Position', [subplot_x_coordinate(2), subplot_y_coordinate(1), subplot_size]);

% link the x-axis of the subplots
    linkaxes([ax1,ax2,ax3,ax4,ax5,ax6],'x');

% reset all fonts and finish the plot
    set(findall(gcf,'-property','FontName'),'FontName',font_name);
    set(findall(gcf,'-property','FontSize'),'FontSize',font_size);
    hold off;

% text
    annotation('textbox',[subplot_x_coordinate(1)-0.045 subplot_y_coordinate(3)+subplot_size(2)-0.015 0 0],'String','\bf{A}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);
    annotation('textbox',[subplot_x_coordinate(1)-0.045 subplot_y_coordinate(2)+subplot_size(2)-0.015 0 0],'String','\bf{B}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);
    annotation('textbox',[subplot_x_coordinate(1)-0.045 subplot_y_coordinate(1)+subplot_size(2)-0.005 0 0],'String','\bf{C}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);
    annotation('textbox',[subplot_x_coordinate(2)-0.058 subplot_y_coordinate(3)+subplot_size(2)-0.015 0 0],'String','\bf{D}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);
    annotation('textbox',[subplot_x_coordinate(2)-0.058 subplot_y_coordinate(2)+subplot_size(2)-0.015 0 0],'String','\bf{E}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);
    annotation('textbox',[subplot_x_coordinate(2)-0.058 subplot_y_coordinate(1)+subplot_size(2)-0.015 0 0],'String','\bf{F}','HorizontalAlignment','center','VerticalAlignment','middle','EdgeColor','none','FontName',font_name,'FontSize',10.0);

%% save plots
    figHandles = get(groot, 'Children');
    for i1 = 1:length(figHandles)
        if ~exist('simName','var')
            exportgraphics(figHandles(uint32(length(figHandles)+1-i1)),['fig_',fig_group_name,'_',sprintf('%02.0f',uint32(i1)),'.png'],'Resolution',600);
        else
            resultsFolder = fullfile('01_Results', simName);
            mkdir(resultsFolder);
            exportgraphics(figHandles(uint32(length(figHandles)+1-i1)),[resultsFolder, '\fig_',fig_group_name,'_',sprintf('%02.0f',uint32(i1)),'.png'],'Resolution',600);
        end
    %   saveas(figHandles(uint32(length(figHandles)+1-i)),['fig_',fig_group_name,'_',sprintf('%02.0f',uint32(i)),'.svg']);
    end; clear i1;
    svg2pdf(cd);
    delete *.svg;

% clear unused variables
    clearvars -except Input Optimization;