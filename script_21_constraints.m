%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%% create special matrix for energy conservation
    t_minus_one_mat = sparse(2:(Input.optimization_time/Input.sample_time),1:(Input.optimization_time/Input.sample_time-1),1);
    t_minus_one_mat(Input.optimization_time/Input.sample_time,Input.optimization_time/Input.sample_time) = 0;

%% grouping of fcr provision period
% this code serves to ensure that the rolling horizon does not have to be a multiple of the fcr provision time
    if Input.FCR.period > Input.sample_time
        period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.FCR.period)/Input.sample_time;
        period_start = Input.FCR.period/Input.sample_time - period_previous; if period_start == Input.FCR.period/Input.sample_time; period_start = 0; end; period_start = min(period_start,Input.optimization_time/Input.sample_time);
        period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.FCR.period/Input.sample_time))*(Input.FCR.period/Input.sample_time); period_normal = max(period_normal,0);
        period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
        opt_problem.Constraints.fcr_cycle = optimconstr(Input.optimization_time/Input.sample_time);
        if period_start ~= 0
            opt_problem.Constraints.fcr_cycle(1:period_start) = ...
                power_fcr_allocation_total(1:period_start) == repelem(Opt.power_fcr_allocation_total(end),period_start,1);
        end
        for i1 = uint32(1:period_normal*Input.sample_time/Input.FCR.period)
            opt_problem.Constraints.fcr_cycle(period_start+(i1-1)*(Input.FCR.period/Input.sample_time)+1:period_start+(i1)*(Input.FCR.period/Input.sample_time)) = ...
                power_fcr_allocation_total(period_start+(i1-1)*(Input.FCR.period/Input.sample_time)+1:period_start+(i1)*(Input.FCR.period/Input.sample_time)) == repelem(power_fcr_allocation_total(period_start+(i1)*(Input.FCR.period/Input.sample_time)),Input.FCR.period/Input.sample_time,1);
        end
        if period_end ~= 0
            opt_problem.Constraints.fcr_cycle(end-period_end+1:end) = ...
                power_fcr_allocation_total(end-period_end+1:end) == repelem(power_fcr_allocation_total(end),period_end,1);
        end
        clear period_* i1;
    end

%% grouping of spot market provision time
% this code serves to ensure that the rolling horizon does not have to be a multiple of the spot market provision period
    if Input.SMT.period > Input.sample_time
        period_previous = mod(double(optimization_loop-1)*Input.rolling_horizon,Input.SMT.period)/Input.sample_time;
        period_start = Input.SMT.period/Input.sample_time - period_previous; if period_start == Input.SMT.period/Input.sample_time; period_start = 0; end; period_start = min(period_start,Input.optimization_time/Input.sample_time);
        period_normal = floor((Input.optimization_time/Input.sample_time-period_start)/(Input.SMT.period/Input.sample_time))*(Input.SMT.period/Input.sample_time); period_normal = max(period_normal,0);
        period_end = Input.optimization_time/Input.sample_time-period_start-period_normal;
        opt_problem.Constraints.smt_cycle_purchase = optimconstr(Input.optimization_time/Input.sample_time);
        opt_problem.Constraints.smt_cycle_sell = optimconstr(Input.optimization_time/Input.sample_time);
        if period_start ~= 0
            opt_problem.Constraints.smt_cycle_purchase(1:period_start) = ...
                power_smt_purchase_total(1:period_start) == repelem(Opt.power_smt_purchase_total(end),period_start,1);
            opt_problem.Constraints.smt_cycle_sell(1:period_start) = ...
                power_smt_sell_total(1:period_start) == repelem(Opt.power_smt_sell_total(end),period_start,1);
        end
        for i1 = uint32(1:period_normal*Input.sample_time/Input.SMT.period)
            opt_problem.Constraints.smt_cycle_purchase(period_start+(i1-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i1)*(Input.SMT.period/Input.sample_time)) = ...
                power_smt_purchase_total(period_start+(i1-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i1)*(Input.SMT.period/Input.sample_time)) == repelem(power_smt_purchase_total(period_start+(i1)*(Input.SMT.period/Input.sample_time)),Input.SMT.period/Input.sample_time,1);
            opt_problem.Constraints.smt_cycle_sell(period_start+(i1-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i1)*(Input.SMT.period/Input.sample_time)) = ...
                power_smt_sell_total(period_start+(i1-1)*(Input.SMT.period/Input.sample_time)+1:period_start+(i1)*(Input.SMT.period/Input.sample_time)) == repelem(power_smt_sell_total(period_start+(i1)*(Input.SMT.period/Input.sample_time)),Input.SMT.period/Input.sample_time,1);
        end
        if period_end ~= 0
            opt_problem.Constraints.smt_cycle_purchase(end-period_end+1:end) = ...
                power_smt_purchase_total(end-period_end+1:end) == repelem(power_smt_purchase_total(end),period_end,1);
            opt_problem.Constraints.smt_cycle_sell(end-period_end+1:end) = ...
                power_smt_sell_total(end-period_end+1:end) == repelem(power_smt_sell_total(end),period_end,1);
        end
        clear period_* i1;
    end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%% profit functions for applications
    opt_problem.Constraints.profit_fcr = ... % profit function for fcr
        profit_fcr == repmat(price_fcr,1,Input.entities) * Input.sample_time .* power_fcr_allocation;
    opt_problem.Constraints.profit_smt = ... % profit function for spot market trading
        profit_smt == repmat((price_smt(:,2) - Input.SMT.charges_sell),1,Input.entities) * Input.sample_time .* power_smt_sell ...
        - repmat((price_smt(:,1) + Input.SMT.charges_purchase),1,Input.entities) * Input.sample_time .* power_smt_purchase;
    opt_problem.Constraints.profit_ps = ... % profit function for ps
        profit_ps == Input.PS.price' .* (max(max(power_load - power_pv)',power_btm_purchase_peak_max)' - power_btm_purchase_peak);
    opt_problem.Constraints.profit_sci = ... % profit function for sci
        profit_sci == repmat(price_sell_btm,1,Input.entities) * Input.sample_time .* power_btm_sell - repmat(price_purchase_btm,1,Input.entities) * Input.sample_time .* power_btm_purchase;

%% economic costs due to cycle degradation of the storage system
    dummy_1 = 1./(2 * Input.EV.energy_nominal); dummy_1(dummy_1==Inf) = 0;
    dummy_2 = (Input.EV.invest_battery ./ Input.EV.efc_capability); dummy_2(isnan(dummy_2)) = 0;
    opt_problem.Constraints.costs_cycle_ev = ...
        costs_cycle_ev == (power_ev_charge + power_ev_discharge) * Input.sample_time ...
        .* repmat(reshape(dummy_1 .* dummy_2,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1);
    clear dummy*;
    dummy_1 = 1./(2 * Input.SESS.energy_nominal); dummy_1(dummy_1==Inf) = 0;
    dummy_2 = (Input.SESS.invest_battery ./ Input.SESS.efc_capability); dummy_2(isnan(dummy_2)) = 0;
    opt_problem.Constraints.costs_cycle_sess = ...
        costs_cycle_sess == (power_sess_charge + power_sess_discharge) * Input.sample_time ...
        .* repmat(dummy_1' .* dummy_2',Input.optimization_time/Input.sample_time,1);
    clear dummy*;

%% energy conservation at the end of optimization period
%     opt_problem.Constraints.energy_ev_conservation_end = ...
%         reshape(energy_ev_usable .* Input.EV.state_of_charge_end,1,Input.entities,Input.vehicles) <= energy_ev_actual(end,:,:);
    opt_problem.Constraints.energy_sess_conservation_end = ...
        reshape(energy_sess_usable .* Input.SESS.state_of_charge_end,1,Input.entities) <= energy_sess_actual(end,:);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

    %% actual energy content
        opt_problem.Constraints.energy_ev_actual = ... % actual energy content of ev
            energy_ev_actual == energy_ev_actual_ftm + energy_ev_actual_btm;
        opt_problem.Constraints.energy_sess_actual = ... % actual energy content of sess
            energy_sess_actual == energy_sess_actual_ftm + energy_sess_actual_btm;

    %% usable/reserved energy content at ev
        opt_problem.Constraints.energy_ev_usable = ... % allocate usable energy
            repmat(reshape(energy_ev_usable,1,Input.entities,Input.vehicles),Input.optimization_time/Input.sample_time,1,1) == energy_ev_usable_ftm + energy_ev_usable_btm;
        opt_problem.Constraints.energy_ev_usable_btm = ... % actual energy content for btm partition
            energy_ev_actual_btm <= energy_ev_usable_btm;
        opt_problem.Constraints.energy_ev_usable_ftm = ... % actual energy content for ftm partition
            energy_ev_actual_ftm <= energy_ev_usable_ftm;
        opt_problem.Constraints.power_ev_charge = ... % power charge
            power_ev_charge == power_ev_charge_ftm + power_ev_charge_btm;
        opt_problem.Constraints.power_ev_discharge = ... % power discharge
            power_ev_discharge == power_ev_discharge_ftm + power_ev_discharge_btm;

    %% usable/reserved energy content at sess
        opt_problem.Constraints.energy_sess_usable = ... % allocate usable energy
            repmat(energy_sess_usable',Input.optimization_time/Input.sample_time,1) == energy_sess_usable_ftm + energy_sess_usable_btm;
        opt_problem.Constraints.energy_sess_usable_btm = ... % actual energy content for btm partition
            energy_sess_actual_btm <= energy_sess_usable_btm;
        opt_problem.Constraints.energy_sess_usable_ftm = ... % actual energy content for ftm partition
            energy_sess_actual_ftm <= energy_sess_usable_ftm;
        opt_problem.Constraints.power_sess_charge = ... % power charge
            power_sess_charge == power_sess_charge_ftm + power_sess_charge_btm;
        opt_problem.Constraints.power_sess_discharge = ... % power discharge
            power_sess_discharge == power_sess_discharge_ftm + power_sess_discharge_btm;

    %% ev: energy conservation at btm partition
    dummy_1 = 1./Input.EV.efficiency_discharge; dummy_1(dummy_1==Inf) = 0;
    opt_problem.Constraints.energy_ev_actual_btm_conservation = energy_ev_actual_btm == 0;
    if Input.ftm2btm_driving
        %% ev: energy conservation at btm partition
        for i1 = uint32(1:Input.vehicles)
            opt_problem.Constraints.energy_ev_actual_btm_conservation(:,:,i1) = ... % energy conservation at btm partition
                energy_ev_actual_btm(:,:,i1) == t_minus_one_mat * energy_ev_actual_btm(:,:,i1) ...
                + power_ev_charge_btm(:,:,i1) * Input.sample_time .* repmat(Input.EV.efficiency_charge(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - power_ev_discharge_btm(:,:,i1) * Input.sample_time .* repmat(dummy_1(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - power_drive_btm(:,:,i1) * Input.sample_time ...
                + (power_ev_charge_extern(:,:,i1) + power_ev_charge_extern_drive(:,:,i1)) * Input.sample_time .* repmat(Input.EV.efficiency_charge(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - repmat(Input.EV.energy_self_discharge(:,i1)',Input.optimization_time/Input.sample_time,1);
        end
        if optimization_loop == 1
            opt_problem.Constraints.energy_ev_actual_btm_conservation(1,:,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_ev_actual_btm(1,:,:) == reshape(energy_ev_actual_initial,1,Input.entities,Input.vehicles) - energy_ev_actual_ftm(1,:,:) ...
                + power_ev_charge_btm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_btm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - power_drive_btm(1,:,:) * Input.sample_time ...
                + (power_ev_charge_extern(1,:,:) + power_ev_charge_extern_drive(1,:,:)) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles);
        else
            opt_problem.Constraints.energy_ev_actual_btm_conservation(1,:,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_ev_actual_btm(1,:,:) == Opt.energy_ev_actual_btm(end,:,:) ...
                + power_ev_charge_btm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_btm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - power_drive_btm(1,:,:) * Input.sample_time ...
                + (power_ev_charge_extern(1,:,:) + power_ev_charge_extern_drive(1,:,:)) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - reshape(Input.EV.energy_self_discharge,1,Input.entities,Input.vehicles);
        end
        
        opt_problem.Constraints.power_drive_btm = ... % power from driving is provided by btm partition and energy shift from ftm partition
            power_drive == power_drive_btm + energy_ev_ftm2btm / Input.sample_time;
    else
        for i1 = uint32(1:Input.vehicles)
            opt_problem.Constraints.energy_ev_actual_btm_conservation(:,:,i1) = ... % energy conservation at btm partition
                energy_ev_actual_btm(:,:,i1) == t_minus_one_mat * energy_ev_actual_btm(:,:,i1) ...
                + power_ev_charge_btm(:,:,i1) * Input.sample_time .* repmat(Input.EV.efficiency_charge(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - power_ev_discharge_btm(:,:,i1) * Input.sample_time .* repmat(dummy_1(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - power_drive(:,:,i1) * Input.sample_time ...
                + (power_ev_charge_extern(:,:,i1) + power_ev_charge_extern_drive(:,:,i1)) * Input.sample_time .* repmat(Input.EV.efficiency_charge(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                + energy_ev_ftm2btm(:,:,i1) ...
                - repmat(Input.EV.energy_self_discharge(:,i1)',Input.optimization_time/Input.sample_time,1);
        end
        if optimization_loop == 1
            opt_problem.Constraints.energy_ev_actual_btm_conservation(1,:,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_ev_actual_btm(1,:,:) == reshape(energy_ev_actual_initial,1,Input.entities,Input.vehicles) - energy_ev_actual_ftm(1,:,:) ...
                + power_ev_charge_btm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_btm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - power_drive(1,:,:) * Input.sample_time ...
                + (power_ev_charge_extern(1,:,:) + power_ev_charge_extern_drive(1,:,:)) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                + energy_ev_ftm2btm(1,:,:);
        else
            opt_problem.Constraints.energy_ev_actual_btm_conservation(1,:,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_ev_actual_btm(1,:,:) == Opt.energy_ev_actual_btm(end,:,:) ...
                + power_ev_charge_btm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_btm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - power_drive(1,:,:) * Input.sample_time ...
                + (power_ev_charge_extern(1,:,:) + power_ev_charge_extern_drive(1,:,:)) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                + energy_ev_ftm2btm(1,:,:) ...
                - reshape(Input.EV.energy_self_discharge,1,Input.entities,Input.vehicles);
        end
        
        opt_problem.Constraints.power_drive_btm = ...
            power_drive_btm == 0;
    end
    clear i1 dummy*;

    % buffer energy of electric vehicle for soc preference
    opt_problem.Constraints.energy_ev_buffer = ...
        energy_ev_actual_btm + energy_ev_buffer >= reshape(Input.EV.soc_preference .* Input.EV.energy_nominal .* state_of_health_ev,1,size(Input.EV.energy_nominal,1),size(Input.EV.energy_nominal,2)) .* ev_available;

    % buffer energy of electric vehicle for soc end
    opt_problem.Constraints.energy_ev_buffer_end = ...
        energy_ev_actual_btm(end,:,:) + energy_ev_actual_ftm(end,:,:) + energy_ev_buffer_end >= reshape(Input.EV.state_of_charge_end .* Input.EV.energy_nominal .* state_of_health_ev,1,size(Input.EV.energy_nominal,1),size(Input.EV.energy_nominal,2));

    % buffer energy of electric vehicle for soc preference
    if Input.soh_caluclation
        opt_problem.Constraints.energy_ev_buffer_lb = ...
            energy_ev_actual_btm + energy_ev_actual_ftm + energy_ev_buffer_lb >= 0.0025 * repmat(reshape(energy_ev_usable,1,size(energy_ev_usable,1),size(energy_ev_usable,2)),Input.optimization_time/Input.sample_time,1,1);
        opt_problem.Constraints.energy_ev_buffer_ub = ...
            energy_ev_actual_btm + energy_ev_actual_ftm - energy_ev_buffer_ub <= (1-0.0025) * repmat(reshape(energy_ev_usable,1,size(energy_ev_usable,1),size(energy_ev_usable,2)),Input.optimization_time/Input.sample_time,1,1);
    end

    %% ev: energy conservation at ftm partition
        dummy_1 = 1./Input.EV.efficiency_discharge; dummy_1(dummy_1==Inf) = 0;
        opt_problem.Constraints.energy_ev_actual_ftm_conservation = energy_ev_actual_ftm == 0;
        for i1 = uint32(1:Input.vehicles)
            opt_problem.Constraints.energy_ev_actual_ftm_conservation(:,:,i1) = ... % energy conservation at ftm partition
                energy_ev_actual_ftm(:,:,i1) == t_minus_one_mat * energy_ev_actual_ftm(:,:,i1) ...
                + power_ev_charge_ftm(:,:,i1) * Input.sample_time .* repmat(Input.EV.efficiency_charge(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - power_ev_discharge_ftm(:,:,i1) * Input.sample_time .* repmat(dummy_1(:,i1)',Input.optimization_time/Input.sample_time,1) ...
                - energy_ev_ftm2btm(:,:,i1);
        end
        if optimization_loop == 1
            opt_problem.Constraints.energy_ev_actual_ftm_conservation(1,:,:) = ... % energy conservation at ftm partition (considering initial energy content)
                energy_ev_actual_ftm(1,:,:) == reshape(energy_ev_actual_initial,1,Input.entities,Input.vehicles) - energy_ev_actual_btm(1,:,:) ...
                + power_ev_charge_ftm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_ftm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - energy_ev_ftm2btm(1,:,:);
        else
            opt_problem.Constraints.energy_ev_actual_ftm_conservation(1,:,:) = ... % energy conservation at ftm partition (considering initial energy content)
                energy_ev_actual_ftm(1,:,:) == Opt.energy_ev_actual_ftm(end,:,:) ...
                + power_ev_charge_ftm(1,:,:) * Input.sample_time .* reshape(Input.EV.efficiency_charge,1,Input.entities,Input.vehicles) ...
                - power_ev_discharge_ftm(1,:,:) * Input.sample_time .* reshape(dummy_1,1,Input.entities,Input.vehicles) ...
                - energy_ev_ftm2btm(1,:,:);
        end
        clear i1 dummy*;

    %% sess: energy conservation at btm partition
        dummy_1 = 1./Input.SESS.efficiency_discharge; dummy_1(dummy_1==Inf) = 0;
        opt_problem.Constraints.energy_sess_actual_btm_conservation = ... % energy conservation at btm partition
            energy_sess_actual_btm == t_minus_one_mat * energy_sess_actual_btm ...
            + power_sess_charge_btm * Input.sample_time .* repmat(Input.SESS.efficiency_charge',Input.optimization_time/Input.sample_time,1) ...
            - power_sess_discharge_btm * Input.sample_time .* repmat(dummy_1',Input.optimization_time/Input.sample_time,1) ...
            + energy_sess_ftm2btm ...
            - repmat(Input.SESS.energy_self_discharge',Input.optimization_time/Input.sample_time,1);
        if optimization_loop == 1
            opt_problem.Constraints.energy_sess_actual_btm_conservation(1,:,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_sess_actual_btm(1,:) == energy_sess_actual_initial' - energy_sess_actual_ftm(1,:) ...
                + power_sess_charge_btm(1,:) * Input.sample_time .* Input.SESS.efficiency_charge' ...
                - power_sess_discharge_btm(1,:) * Input.sample_time .* dummy_1' ...
                + energy_sess_ftm2btm(1,:) ...
                - Input.SESS.energy_self_discharge';
        else
            opt_problem.Constraints.energy_sess_actual_btm_conservation(1,:) = ... % energy conservation at btm partition (considering initial energy content)
                energy_sess_actual_btm(1,:) == Opt.energy_sess_actual_btm(end,:) ...
                + power_sess_charge_btm(1,:) * Input.sample_time .* Input.SESS.efficiency_charge' ...
                - power_sess_discharge_btm(1,:) * Input.sample_time .* dummy_1' ...
                + energy_sess_ftm2btm(1,:) ...
                - Input.SESS.energy_self_discharge';
        end
        clear i1 dummy*;

    %% sess: energy conservation at ftm partition
        dummy_1 = 1./Input.SESS.efficiency_discharge; dummy_1(dummy_1==Inf) = 0;
        opt_problem.Constraints.energy_sess_actual_ftm_conservation = ... % energy conservation at ftm partition
            energy_sess_actual_ftm == t_minus_one_mat * energy_sess_actual_ftm ...
            + power_sess_charge_ftm * Input.sample_time .* repmat(Input.SESS.efficiency_charge',Input.optimization_time/Input.sample_time,1) ...
            - power_sess_discharge_ftm * Input.sample_time .* repmat(dummy_1',Input.optimization_time/Input.sample_time,1) ...
            - energy_sess_ftm2btm;
        if optimization_loop == 1
            opt_problem.Constraints.energy_sess_actual_ftm_conservation(1,:,:) = ... % energy conservation at ftm partition (considering initial energy content)
                energy_sess_actual_ftm(1,:) == energy_sess_actual_initial' - energy_sess_actual_btm(1,:) ...
                + power_sess_charge_ftm(1,:) * Input.sample_time .* Input.SESS.efficiency_charge' ...
                - power_sess_discharge_ftm(1,:) * Input.sample_time .* dummy_1' ...
                - energy_sess_ftm2btm(1,:);
        else
            opt_problem.Constraints.energy_sess_actual_ftm_conservation(1,:) = ... % energy conservation at ftm partition (considering initial energy content)
                energy_sess_actual_ftm(1,:) == Opt.energy_sess_actual_ftm(end,:) ...
                + power_sess_charge_ftm(1,:) * Input.sample_time .* Input.SESS.efficiency_charge' ...
                - power_sess_discharge_ftm(1,:) * Input.sample_time .* dummy_1' ...
                - energy_sess_ftm2btm(1,:);
        end
        clear i1 dummy*;

    %% btm: power balance at btm node
        opt_problem.Constraints.btm_node = ... % power balance at btm node
            power_pv + sum(power_ev_discharge_btm,3) + power_sess_discharge_btm + power_btm_purchase == power_load + sum(power_ev_charge_btm,3) + power_sess_charge_btm + power_btm_sell + power_btm_curtailment;

    %% ftm: power balance at ftm node
        opt_problem.Constraints.power_ftm = ... % actually required power for fcr and spot market trading (ftm node)
            sum(power_ev_discharge_ftm,3) + power_sess_discharge_ftm + power_smt_purchase == power_fcr_actual + power_smt_sell + sum(power_ev_charge_ftm,3) + power_sess_charge_ftm + power_ftm_curtailment;
    
%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%% constraint for ps application
% peak shaving threshold
    if config.ps_power(iConfig) > 0
        opt_problem.Constraints.power_btm_purchase_peak = ... % calculate maximum power value purchased from grid
        repmat(power_btm_purchase_peak,Input.optimization_time/Input.sample_time,1) >= power_btm_purchase - power_btm_sell + power_smt_purchase - power_smt_sell - power_fcr_actual;

        opt_problem.Constraints.power_purchase_peak = ... % calculate maximum power value purchased from grid
            config.ps_power(iConfig) == power_btm_purchase_peak;
    else
        opt_problem.Constraints.power_btm_purchase_peak = ... % calculate maximum power value purchased from grid
            repmat(power_btm_purchase_peak,Input.optimization_time/Input.sample_time,1) >= power_btm_purchase - power_btm_sell;% + power_smt_purchase - power_smt_sell - power_fcr_actual;
    end

%% constraint for sci application
% feed-in limitation
    opt_problem.Constraints.power_feedin_limit = ... % feed-in limitation for power flow from btm node to grid
        power_btm_sell <= repmat(Input.SCI.feedin_limit',Input.optimization_time/Input.sample_time,1);

%% constraint for fcr provision
% fcr: required power for fcr provision (considering the frequency, dead band, and overfulfillment)
    opt_problem.Constraints.power_fcr_allocation_min = ... % minimum allocated power for fcr provision
        power_fcr_allocation .* repmat(fcr_power_bounds(:,1),1,Input.entities) <= power_fcr_actual;
    opt_problem.Constraints.power_fcr_allocation_max = ... % maximum allocated power for fcr provision
        power_fcr_actual <= power_fcr_allocation .* repmat(fcr_power_bounds(:,2),1,Input.entities);

    % fcr: limited usable energy to fulfill reserve time criteria at fcr provision
        opt_problem.Constraints.fcr_e_min = ... % minimum energy content for reserve time criteria
            Input.FCR.reserve_time * power_fcr_allocation <= sum(energy_ev_actual_ftm,3) + energy_sess_actual_ftm;
        opt_problem.Constraints.fcr_e_max = ... % maximum energy content for reserve time criteria
            sum(energy_ev_actual_ftm,3) + energy_sess_actual_ftm <= sum(energy_ev_usable_ftm,3) + energy_sess_usable_ftm - Input.FCR.reserve_time * power_fcr_allocation;

% fcr: limited usable power
    opt_problem.Constraints.fcr_p_min_1 = ... % minimum power reserve during charging
        sum(reshape(Input.EV.power_charge,1,Input.entities,Input.vehicles).*ev_available,3) + repmat(Input.SESS.power_charge',Input.optimization_time/Input.sample_time,1) ... 
        >= power_fcr_allocation * (1+Input.FCR.reserve_power);
    opt_problem.Constraints.fcr_p_min_2 = ... % minimum power reserve during discharging
        sum(reshape(Input.EV.power_discharge,1,Input.entities,Input.vehicles).*ev_available,3) + repmat(Input.SESS.power_discharge',Input.optimization_time/Input.sample_time,1) ... 
        >= power_fcr_allocation * (1+Input.FCR.reserve_power);

% fcr: total allocated power
    opt_problem.Constraints.power_fcr_allocation_total = ...
        power_fcr_allocation_total == sum(power_fcr_allocation,2);    

    % fcr: create a binary for fcr provision
        opt_problem.Constraints.active_fcr_lb = ... % lower bound of binary fcr variable
            0 <= active_fcr * Input.big_M - power_fcr_allocation_total;
        opt_problem.Constraints.active_fcr_ub = ... % upper bound of binary fcr variable
            active_fcr * Input.big_M - power_fcr_allocation_total <= Input.big_M;

    % fcr: consider minimum power
        opt_problem.Constraints.power_fcr_minimum = ...
            active_fcr .* Input.FCR.power_minimum <= power_fcr_allocation_total;

%     % fcr: consider increments of power
%         opt_problem.Constraints.power_increment_fcr = ...
%             increment_fcr .* Input.FCR.power_minimum == power_fcr_allocation_total;

%% constraint for smt application
% smt: total allocated power
    opt_problem.Constraints.power_smt_sell_total = ...
        power_smt_sell_total == sum(power_smt_sell,2);
    opt_problem.Constraints.power_smt_purchase_total = ...
        power_smt_purchase_total == sum(power_smt_purchase,2);

    % smt: create a binary for spot market trading
        opt_problem.Constraints.active_smt_purchase_lb = ... % lower bound of binary smt variable at electricity purchase
            0 <= active_smt_purchase * Input.big_M - power_smt_purchase_total;
        opt_problem.Constraints.active_smt_purchase_ub = ... % upper bound of binary smt variable at electricity purchase
            active_smt_purchase * Input.big_M - power_smt_purchase_total <= Input.big_M;
        opt_problem.Constraints.active_smt_sell_lb = ... % lower bound of binary smt variable at electricity sale
            0 <= active_smt_sell * Input.big_M - power_smt_sell_total;
        opt_problem.Constraints.active_smt_sell_ub = ... % upper bound of binary smt variable at electricity sale
            active_smt_sell * Input.big_M - power_smt_sell_total <= Input.big_M;

    % smt: consider minimum power
        opt_problem.Constraints.power_smt_sell_minimum = ...
            active_smt_sell .* Input.SMT.power_minimum <= power_smt_sell_total;
        opt_problem.Constraints.power_smt_purchase_minimum = ...
            active_smt_purchase .* Input.SMT.power_minimum <= power_smt_purchase_total;

%     % smt: consider increments of power
%         opt_problem.Constraints.power_increment_smt_sell = ...
%             increment_smt_sell .* Input.SMT.power_minimum == power_smt_sell_total;
%         opt_problem.Constraints.power_increment_smt_purchase = ...
%             increment_smt_purchase .* Input.SMT.power_minimum == power_smt_purchase_total;

    % smt: exclusive energy sell and purchase
        opt_problem.Constraints.power_smt_trade_exclusive = ...
            active_smt_sell + active_smt_purchase <= 1;
    
    if Input.active_fcr_st_smt
         opt_problem.Constraints.active_fcr_st_smt_1 = ...
             active_smt_sell <= active_fcr;
         opt_problem.Constraints.active_fcr_st_smt_2 = ...
             active_smt_purchase <= active_fcr;
    end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

%% reset decision variables if specific applications are disabled
if ~Input.EV.energy_shift(1,1,1) && Input.EV.power_discharge(1,1,1) == 0
    opt_problem.Constraints.disable_ftm_bid = energy_ev_usable_ftm == 0;
end

if ~Input.active_fcr
    opt_problem.Constraints.disable_fcr = power_fcr_allocation == 0;
end

if ~Input.active_smt && ~(Input.active_fcr && Input.active_fcr_st_smt)
    opt_problem.Constraints.disable_smt_1 = power_smt_purchase_total == 0;
    opt_problem.Constraints.disable_smt_2 = power_smt_sell_total == 0;
end

if ~Input.active_smt && ~Input.active_fcr
    opt_problem.Constraints.disable_ftm_1 = energy_ev_usable_ftm == 0;
    opt_problem.Constraints.disable_ftm_2 = energy_ev_ftm2btm == 0;
    opt_problem.Constraints.disable_ftm_3 = energy_sess_usable_ftm == 0;
    opt_problem.Constraints.disable_ftm_4 = energy_sess_ftm2btm == 0;
end

if ~Input.active_ps && ~Input.active_sci
    opt_problem.Constraints.disable_btm_1 = energy_ev_usable_btm == 0;
    opt_problem.Constraints.disable_btm_2 = energy_sess_usable_btm == 0;
end

clear t_minus_one_mat Input.big_M;