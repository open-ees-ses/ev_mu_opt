%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

%% power supply [kW]
    for j1 = 1:Input.entities
        if Input.peak_generation(j1) > 0
            try
                load(Input.path_profile,['profile_power_gen_',sprintf('%03d',Input.number_pv(j1))]);
                profile_power_gen = eval(['profile_power_gen_',sprintf('%03d',Input.number_pv(j1)),';']);
                profile_power_gen = repelem(double(profile_power_gen),1,1); % call generation profile
                profile_power_gen = profile_power_gen / max(profile_power_gen) * Input.peak_generation(j1); % scaling of profile
                profile_power_gen = repmat(profile_power_gen,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
                dummy = zeros(Input.profile_length,1);
                for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(profile_power_gen(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
                Input.power_pv(:,j1) = dummy; clear dummy;
                clear profile*;
            catch
                Input.power_pv(:,j1) = zeros(Input.profile_length,1);
            end
        else
            Input.power_pv(:,j1) = zeros(Input.profile_length,1);
        end
    end; clear i1 j1;

%% active power demand [kW]
    for j1 = 1:Input.entities
        if Input.consumption_building(j1) > 0
            try
                load(Input.path_profile,['profile_power_active_load_',sprintf('%03d',Input.number_power_load(j1))]);
                profile_power_load = eval(['profile_power_active_load_',sprintf('%03d',Input.number_power_load(j1)),';']);
                profile_power_load = repelem(double(profile_power_load),1,1); % call load power profile
                profile_power_load = profile_power_load / sum(profile_power_load) * Input.consumption_building(j1) * 60; % scaling of profile
                profile_power_load = repmat(profile_power_load,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
                dummy = zeros(Input.profile_length,1);
                for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(profile_power_load(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
                Input.power_load(:,j1) = dummy; clear dummy;
                clear profile*;
            catch
                Input.power_load(:,j1) = zeros(Input.profile_length,1);
            end
        else
            Input.power_load(:,j1) = zeros(Input.profile_length,1);
        end
    end; clear i1 j1;

%% ev profiles
    try load('ev_profiles.mat'); catch; end
    for j1 = 1:Input.entities
        for k = 1:Input.vehicles
            if Input.active_ev(j1,k) == 1
                try
                    ev_drive_distance = profile_ev_drive_distance(:,Input.number_ev_profile(j1,k)) / sum(profile_ev_drive_distance(:,Input.number_ev_profile(j1,k))) * Input.EV.avg_distance(j1,k);
                    ev_available = profile_ev_plugged_work(:,Input.number_ev_profile(j1,k));
                    ev_available_2 = profile_ev_plugged_home(:,Input.number_ev_profile(j1,k));
                    ev_drive_distance = repelem(double(ev_drive_distance),15,1) / 15; % call profile
                    ev_available = repelem(double(ev_available),15,1); % call profile
                    ev_available_2 = repelem(double(ev_available_2),15,1); % call profile
                    ev_drive_distance = repmat(ev_drive_distance,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
                    ev_available = repmat(ev_available,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
                    ev_available_2 = repmat(ev_available_2,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
                    dummy_1 = zeros(Input.profile_length,1);
                    dummy_2 = zeros(Input.profile_length,1);
                    dummy_3 = zeros(Input.profile_length,1);
                    for i1 = uint32(1:Input.profile_length)
                        dummy_1(i1) = sum(ev_drive_distance(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60))));
                        dummy_2(i1) = mean(ev_available(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60))));
                        dummy_3(i1) = mean(ev_available_2(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60))));
                    end; clear i1;
                    Input.power_drive(:,j1,k) = dummy_1 * Input.EV.avg_consumption(j1,k) / Input.sample_time;
                    Input.ev_available(:,j1,k) = dummy_2;
                    Input.ev_available_2(:,j1,k) = dummy_3;
                    clear dummy* ev*;
                catch
                    Input.power_drive(:,j1,k) = zeros(Input.profile_length,1);
                    Input.ev_available(:,j1,k) = zeros(Input.profile_length,1);
                    Input.ev_available_2(:,j1,k) = zeros(Input.profile_length,1);
                end
            else
                Input.power_drive(:,j1,k) = zeros(Input.profile_length,1);
                Input.ev_available(:,j1,k) = zeros(Input.profile_length,1);
                Input.ev_available_2(:,j1,k) = zeros(Input.profile_length,1);
            end
        end
    end; clear i1 j1 k profile*;

%% fcr price signal [EUR/kWh]
    if Input.active_fcr
        try
            load(Input.path_profile,['profile_price_fcr_',sprintf('%03d',Input.number_price_fcr)]);
            profile_price_fcr = eval(['profile_price_fcr_',sprintf('%03d',Input.number_price_fcr),';']);
            Input.price_fcr = repelem(double(profile_price_fcr),1,1); % [EUR/kWh] % call fcr price profile
            Input.price_fcr = repmat(Input.price_fcr,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
            dummy = zeros(Input.profile_length,1);
            for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(Input.price_fcr(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
            Input.price_fcr = dummy; clear dummy;
            clear profile*;
        catch
            Input.price_fcr = zeros(Input.profile_length,1);
        end
    else
        Input.price_fcr = zeros(Input.profile_length,1);
    end

%% spot market price signal [EUR/kWh]
    if Input.active_smt || (Input.active_fcr && Input.active_fcr_st_smt)
        try
            load(Input.path_profile,['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_low)]);
            profile_price_smt_low = eval(['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_low),';']);
            profile_price_smt_low = repelem(double(profile_price_smt_low),1,1); % [EUR/kWh] % call spot market price profile
            profile_price_smt_low = repmat(profile_price_smt_low,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
            dummy = zeros(Input.profile_length,1);
            for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(profile_price_smt_low(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
            profile_price_smt_low = dummy; clear dummy;
            load(Input.path_profile,['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_high)]);
            profile_price_smt_high = eval(['profile_price_',Input.type_price_smt,'_',sprintf('%03d',Input.number_price_smt_high),';']);
            profile_price_smt_high = repelem(double(profile_price_smt_high),1,1); % [EUR/kWh] % call spot market price profile
            profile_price_smt_high = repmat(profile_price_smt_high,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
            dummy = zeros(Input.profile_length,1);
            for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(profile_price_smt_high(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
            profile_price_smt_high = dummy; clear dummy;
            Input.price_smt = [profile_price_smt_low,profile_price_smt_high];
            clear profile_price_smt_low profile_price_smt_high;
            clear profile*;
        catch
            Input.price_smt = zeros(Input.profile_length,2);
        end
    else
        Input.price_smt = zeros(Input.profile_length,2);
    end

%% frequency signal [Hz]
    if Input.active_fcr
        try
            load(Input.path_profile,['profile_frequency_',sprintf('%03d',Input.number_frequency)]);
            profile_frequency = eval(['profile_frequency_',sprintf('%03d',Input.number_frequency),';']);
            Input.frequency = double(profile_frequency); % [Hz] % call frequency profile
            Input.frequency = repmat(Input.frequency,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1); % [Hz] % call frequency profile
            dummy = zeros(Input.profile_length,1);
            for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(Input.frequency(Input.simulation_start*(60*60)+(i1-1)*(Input.sample_time*(60*60))+1:Input.simulation_start*(60*60)+i1*(Input.sample_time*(60*60)))); end; clear i1;
            Input.frequency = dummy; clear dummy;
            clear profile*;
        catch
            Input.frequency = Input.FCR.frequency_nominal * ones(Input.profile_length,1);
        end
    else
        Input.frequency = Input.FCR.frequency_nominal * ones(Input.profile_length,1);
    end

%% calculation of fcr bounds (minimum and maximum)
    frequency_delta = Input.frequency - Input.FCR.frequency_nominal; % [Hz]
    fcr_power_min = zeros(size(Input.frequency));
    fcr_power_max = zeros(size(Input.frequency));

    for i1 = uint32(1:length(Input.frequency))
        if frequency_delta(i1) >= 0
            if abs(frequency_delta(i1)) <= Input.FCR.frequency_deadband 
                fcr_power_min(i1) = 0;
            else
                fcr_power_min(i1) = max(-1, -(frequency_delta(i1) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop));
            end
            fcr_power_max(i1) = max(-1, -(frequency_delta(i1) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop)) * Input.FCR.overfulfillment; 
        else
            if abs(frequency_delta(i1)) <= Input.FCR.frequency_deadband 
                fcr_power_min(i1) = 0;
            else
                fcr_power_min(i1) = min(1, -(frequency_delta(i1) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop));
            end
            fcr_power_max(i1) = min(1, -(frequency_delta(i1) * 1) / (Input.FCR.frequency_nominal * Input.FCR.frequency_droop)) * Input.FCR.overfulfillment;
        end
    end
    
    Input.fcr_power_bounds = [min(fcr_power_min,fcr_power_max),max(fcr_power_min,fcr_power_max)];
    clear i1 frequency_delta fcr*;

%% temperature data
    try
        load('ambient_temperature_muc_2018.mat');
        Input.SESS.temperature_cell = 25 * ones(Input.profile_length,1) + 273.15; % [K] cell temperature
        Input.EV.temperature_cell = repelem(ambient_temperature_muc_2018,1/Input.sample_time,1) + 5 + 273.15; % [K] cell temperature
        Input.EV.temperature_cell = repelem(double(Input.EV.temperature_cell),Input.sample_time*60,1); % Generate minute profile
        Input.EV.temperature_cell = repmat(Input.EV.temperature_cell,ceil((Input.simulation_start+Input.simulation_time+Input.optimization_time-Input.rolling_horizon)/24/365),1);
        dummy = zeros(Input.profile_length,1);
        for i1 = uint32(1:Input.profile_length); dummy(i1) = mean(Input.EV.temperature_cell(Input.simulation_start*(1*60)+(i1-1)*(Input.sample_time*(1*60))+1:Input.simulation_start*(1*60)+i1*(Input.sample_time*(1*60)))); end; clear i1;
        Input.EV.temperature_cell = dummy;
        clear dummy ambient_temperature_muc_2018;
    catch
        Input.SESS.temperature_cell = 25 * ones(Input.profile_length,1) + 273.15; % [K] cell temperature
     	Input.EV.temperature_cell = 15 * ones(Input.profile_length,1) + 273.15; % [K] cell temperature
    end
