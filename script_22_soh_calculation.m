%% Script Description
%   Coded and provided by:
%   Stefan Englberger (stefan.englberger@tum.de)
%   Institute for Electrical Energy Storage Technology
%   Technical University of Munich
%
% Stefan Englberger, Kareem Abo Gamra, Benedikt Tepe, Michael Schreiber, Andreas Jossen, 
% Holger Hesse. (2021). Electric vehicle multi-use: Optimizing multiple value streams using 
% mobile storage systems in a vehicle-to-grid context. Applied Energy, 304.
% https://doi.org/10.1016/j.apenergy.2021.117862

%% state of charge calculation
for entity = 1:Input.entities
    if Input.soh_caluclation && Input.active_sess(entity)==1
        state_of_charge_sess = Optimization.energy_sess_actual(:,entity) ./ Input.SESS.energy_nominal(entity);
            state_of_charge_sess(isnan(state_of_charge_sess)) = 0;
        temperature_sess = temperature_cell_sess;
        if strcmp(Input.SESS.cell_chemistry(entity),'LFP')
            if optimization_loop == 1
                capacity_loss_sess_total = 1-state_of_health_sess(entity);
                capacity_loss_sess_calendar = capacity_loss_sess_total * 2/3;
            else
                capacity_loss_sess_total = sum(Opt.capacity_loss_sess_total(:,entity));
                capacity_loss_sess_calendar = sum(Opt.capacity_loss_sess_calendar(:,entity));                
            end
            [Q_loss_calendar,Q_loss_cycle] = script_22_soh_calculation_lfp(...
                state_of_charge_sess,...
                temperature_sess,...
                capacity_loss_sess_total,...
                capacity_loss_sess_calendar,...
                Input.sample_time);
            Optimization.capacity_loss_sess_calendar(:,entity) = Q_loss_calendar;
            Optimization.capacity_loss_sess_cycle(:,entity) = Q_loss_cycle;
            clear Q_loss*;
        elseif strcmp(Input.SESS.cell_chemistry(entity),'NMC')
            time = (1+double(optimization_loop-1)*(Input.rolling_horizon/Input.sample_time):1:double(optimization_loop)*(Input.rolling_horizon/Input.sample_time))' * Input.sample_time / 24;
            time_previous = (double(optimization_loop)-1)*(Input.rolling_horizon/Input.sample_time) * Input.sample_time / 24;
            if optimization_loop == 1
                charge_throughput_previous = 0;
            else
                state_of_charge = Optimization.energy_sess_actual(:,entity) ./ Input.SESS.energy_nominal(entity); state_of_charge(isnan(state_of_charge)) = 0;
                state_of_charge_previous = Opt.energy_sess_actual(:,entity) ./ Input.SESS.energy_nominal(entity); state_of_charge(isnan(state_of_charge)) = 0;
                depth_of_discharge_previous = abs([0;diff(state_of_charge_previous)]);
                charge_throughput_previous = sum(Input.SESS.capacity_cell_nominal * depth_of_discharge_previous);
            end
            [Q_loss_calendar,Q_loss_cycle] = script_22_soh_calculation_nmc(...
                state_of_charge_sess,...
                temperature_sess,...
                time,...
                time_previous,...
                charge_throughput_previous,...
                Input.SESS.capacity_cell_nominal);
            Optimization.capacity_loss_sess_calendar(:,entity) = Q_loss_calendar;
            Optimization.capacity_loss_sess_cycle(:,entity) = Q_loss_cycle;
            clear Q_loss*;
        end
    else
        Optimization.capacity_loss_sess_calendar(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);
        Optimization.capacity_loss_sess_cycle(:,entity) = zeros(Input.rolling_horizon/Input.sample_time,1);        
    end

    for vehicle = 1:Input.vehicles
        if Input.soh_caluclation && Input.active_ev(entity)==1
            state_of_charge_ev = Optimization.energy_ev_actual(:,entity,vehicle) ./ Input.EV.energy_nominal(entity,vehicle);
                state_of_charge_ev(isnan(state_of_charge_ev)) = 0;
            temperature_ev = temperature_cell_ev;
            if strcmp(Input.EV.cell_chemistry(entity,vehicle),'LFP')
                if optimization_loop == 1
                    capacity_loss_ev_total = 1-state_of_health_ev(entity,vehicle);
                    capacity_loss_ev_calendar = capacity_loss_ev_total * 2/3;
                else
                    capacity_loss_ev_total = sum(Opt.capacity_loss_ev_total(:,entity,vehicle));
                    capacity_loss_ev_calendar = sum(Opt.capacity_loss_ev_calendar(:,entity,vehicle));                
                end
                [Q_loss_calendar,Q_loss_cycle] = script_22_soh_calculation_lfp(...
                    state_of_charge_ev,...
                    temperature_ev,...
                    capacity_loss_ev_total,...
                    capacity_loss_ev_calendar,...
                    Input.sample_time);
                Optimization.capacity_loss_ev_calendar(:,entity,vehicle) = Q_loss_calendar;
                Optimization.capacity_loss_ev_cycle(:,entity,vehicle) = Q_loss_cycle;
                clear Q_loss*;
            elseif strcmp(Input.EV.cell_chemistry(entity,vehicle),'NMC')
                time = (1+double(optimization_loop-1)*(Input.rolling_horizon/Input.sample_time):1:double(optimization_loop)*(Input.rolling_horizon/Input.sample_time))' * Input.sample_time / 24;
                time_previous = (double(optimization_loop)-1)*(Input.rolling_horizon/Input.sample_time) * Input.sample_time / 24;
                if optimization_loop == 1
                    charge_throughput_previous = 0;
                else
                    state_of_charge = Optimization.energy_ev_actual(:,entity,vehicle) ./ Input.EV.energy_nominal(entity,vehicle); state_of_charge(isnan(state_of_charge)) = 0;
                    state_of_charge_previous = Opt.energy_ev_actual(:,entity,vehicle) ./ Input.EV.energy_nominal(entity,vehicle); state_of_charge(isnan(state_of_charge)) = 0;
                    depth_of_discharge_previous = abs([0;diff(state_of_charge_previous)]);
                    charge_throughput_previous = sum(Input.EV.capacity_cell_nominal * depth_of_discharge_previous);
                end            
                [Q_loss_calendar,Q_loss_cycle] = script_22_soh_calculation_nmc(...
                    state_of_charge_ev,...
                    temperature_ev,...
                    time,...
                    time_previous,...
                    charge_throughput_previous,...
                    Input.EV.capacity_cell_nominal);
                Optimization.capacity_loss_ev_calendar(:,entity,vehicle) = Q_loss_calendar;
                Optimization.capacity_loss_ev_cycle(:,entity,vehicle) = Q_loss_cycle;
                clear Q_loss*;
            end
        else
            Optimization.capacity_loss_ev_calendar(:,entity,vehicle) = zeros(Input.rolling_horizon/Input.sample_time,1);
            Optimization.capacity_loss_ev_cycle(:,entity,vehicle) = zeros(Input.rolling_horizon/Input.sample_time,1);        
        end
    end
end
clear entity state_of_charge* temperature* time charge_throughput_previous capacity_loss* depth_of_discharge vehicle;

%%
% superposition of calendar and cycle degradation
    Optimization.capacity_loss_sess_total = Optimization.capacity_loss_sess_calendar + Optimization.capacity_loss_sess_cycle;
    Optimization.capacity_loss_ev_total = Optimization.capacity_loss_ev_calendar + Optimization.capacity_loss_ev_cycle;

% state of health calculation
    Optimization.state_of_health_sess = repmat(state_of_health_sess',Input.rolling_horizon/Input.sample_time,1) - cumsum(Optimization.capacity_loss_sess_total,1);
    Optimization.state_of_health_ev = permute(repmat(state_of_health_ev,1,1,Input.rolling_horizon/Input.sample_time),[3,1,2]) - cumsum(Optimization.capacity_loss_ev_total,1);
    
clear state_of_health*;